For documentation see the [Wiki](https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/mtlg/module/distributeddisplays).

Trying out the demo:
1. Go into the demo folder: 
    ```
    cd demo
    ```
 
2. Installing node modules
    ```
    npm install
    ```
   
3. Start the game with the serve module command. The 'module' is important.
    ```
    "./node_modules/.bin/mtlg" serve module
    ```

4. Now you can see the game under "localhost:8080".
