/**
 * @Date:   2019-05-29T10:47:53+02:00
 * @Last modified time: 2019-08-14T11:41:28+02:00
 */

 // for easier use
 var locText = MTLG.lang.getString;

 var roomName = "demoQRCode";

 /**
  * Function that checks if level1 should be the next level
  * @param the current game state (an object containing only nextLevel in this case)
  * @return a number in [0,1] that represents that possibility.
  */
 var checkLevel1 = function(gameState){
   if (gameState && gameState.nextLevel && gameState.nextLevel == "qrcode") {
     return 1;
   }
   return 0;
 }



 /**
  * Initialize the first field: Room selection
  */
 var level1_init = function () {
     var location = window.location;
     if(location.search.indexOf('joinRoom') !== -1) {
       var params = parse_query_string(window.location.search.substring(1));
       roomName = params['joinRoom'];
       drawField1(false);
     } else {
       var url = "";
       var parameter = "joinRoom=" + roomName;
       if(location.search == "") {
         url = location.href + "?" + parameter;
       } else {
         url = location.href + "&" + parameter;
       }
       MTLG.distributedDisplays.rooms.createRoom(roomName, function(result){
         if(result && result.success){
           drawField1(true, url);
         }else{
           console.error("Couldn't create room.", result.reason);
           MTLG.lc.goToMenu();
         }
       });
     }
 };

 /**
  * Draw the first field: Room selection
  */
 var drawField1 = function (master, url) {
     let stage = MTLG.getStageContainer();

     // Background
     var background = new createjs.Shape();
     background.graphics.beginFill("#FFF").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);

     var requestButton;
     // Text
     // the text for the title depends on the role.
     var titleText;
     if(master) titleText = locText("master");
     else titleText = locText("slave");

     var title = new createjs.Text(titleText, "40px Arial", "Black");
     title.textBaseline = 'alphabetic';
     title.textAlign = 'center';
     title.x = 0.5 * MTLG.getOptions().width;
     title.y = 0.1 * MTLG.getOptions().height;



     var backButton = MTLG.utils.uiElements.addButton({
       text: locText("back"),
       sizeX: MTLG.getOptions().width * 0.1,
       sizeY: 70,
       textColor: 'black',
       bgColor1: 'white',
       bgColor2: 'red'
     }, function(){
       // Leave the room and delete the sharedObject, so that no updates will be
       // received in the future.
       MTLG.distributedDisplays.rooms.leaveRoom(roomName, function (result) {
           if (result && result.success) {
             var index = location.search.indexOf('joinRoom');
             if(index !== -1) {
               location.search = location.search.substring(0, index) + location.search.substring(index + 'joinRoom='.length + roomName.length + 1);
             } else {
               MTLG.lc.goToMenu();
             }
           } else {
               swal({
                   type: 'error',
                   title: locText("field1_leaveRoomErrorTitle"),
                   text: result.reason,
                   focusConfirm: true
               });
           }
       });

     });
     backButton.x =  MTLG.getOptions().width - MTLG.getOptions().width * 0.1;
     backButton.y = MTLG.getOptions().height - 70;

     // Add objects to stage
     stage.addChild(background);
     stage.addChild(title);

     //Back button to go to menu
     stage.addChild(backButton);

     if (master) {
       // Create QRCode
       MTLG.utils.qrcode.createQRCode(url, function(result) {
         if(result.success) {
           MTLG.log("module", "qr", result);
           // get bounds for positioning
           var bounds = result.bitmap.getBounds();
           result.bitmap.x = MTLG.getOptions().width / 3 - bounds.width / 2;
           result.bitmap.y = MTLG.getOptions().height / 2 - bounds.height / 2;

           // add to stage
           stage.addChild(result.bitmap);
         } else {
           MTLG.log("QRDemo", "The qrcode couldn't be created.", result.reason);
         }

       }, {width: MTLG.getOptions().width / 3}); // set manually options like width
     } else {

     }
 };

 /**
  * This function returns an array with all key value pairs of the url parameters
  *
  * @param  {String} query String from url after "?"
  * @return {object}       Array of key value pairs.
  */
 var parse_query_string = function (query) {
   var vars = query.split("&");
   var query_string = {};
   for (var i = 0; i < vars.length; i++) {
     var pair = vars[i].split("=");
     var key = decodeURIComponent(pair[0]);
     var value = decodeURIComponent(pair[1]);
     // If first entry with this name
     if (typeof query_string[key] === "undefined") {
       query_string[key] = decodeURIComponent(value);
     }
   }
   return query_string;
 }

 module.exports = {
   checkLevel1: checkLevel1,
   level1_init: level1_init
 }
