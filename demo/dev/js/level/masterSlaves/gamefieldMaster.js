/**
 * @Date:   2019-03-26T13:20:42+01:00
 * @Last modified time: 2019-05-15T16:44:12+02:00
 */
 try {
    var utils = require('./utils.js');
 } catch (error) {
   utils = utils || {};
 }

 var locText = MTLG.lang.getString;

 // gobal variables
 var infos;
 var linkedAreas;
 /**
  * Function that checks if this level should be the next level
  * @param the current game state (an object containing only nextLevel in this case)
  * @return a number in [0,1] that represents that possibility.
  */
 var checkGamefield = function(gameState){
   if (gameState && gameState.nextLevel && gameState.nextLevel == "basicMasters.gamefieldMaster") {
     return 1;
   }
   return 0;
 }


 /**
  *  This function initialises the info variable and calls the draw function
  *
  * @param  {type} gameState Current state of the game (should include the 'infos' variable)
  */
 var gamefield_init = function(gameState){
   infos = {};
   linkedAreas = {};
   if(gameState.infos) {
     infos = gameState.infos;
     MTLG.distributedDisplays.actions.registerListenerForAction('updateSharedObject', updateSharedInfos);
   } else {
     // Request Infos

   }
   // ad listener to be able to react on changes of shared objects.
   MTLG.distributedDisplays.actions.registerListenerForAction('addSharedObjects', addSharedObjectsListener);
   draw();
 }


 /**
  * This function draws the gamefield for the maser
  *
  */
 var draw = function () {
   var stage = MTLG.getStageContainer();
   var gWidth = MTLG.getOptions().width;
   var gHeight = MTLG.getOptions().height;
   var settings = MTLG.getSettings().default;
   // White Background Rectangle
   var background = new createjs.Shape();
   background.graphics.beginFill('white').drawRect(0, 0, gWidth, gHeight);
   coordinates = {
     xCoord: gWidth / 2,
     yCoord: gHeight / 2
   };
   background.regX = gWidth / 2;
   background.regY = gHeight / 2;
   background.x = coordinates.xCoord;
   background.y = coordinates.yCoord;
   stage.addChild(background);

   // Add empty LinkedArea(no receiver) as discardPile
   discardPile = new MTLG.distributedDisplays.LinkedArea(20, gHeight - 20 - settings.cards_height, settings.cards_width, settings.cards_height, true, "", false, true);

   // Button to add a card
   var addCardButton = MTLG.utils.uiElements.addButton({
     text: locText("addCard"),
     sizeX: gWidth * 0.1,
     sizeY: 70,
     textColor: 'black',
     bgColor1: 'white',
     bgColor2: 'red'
   }, function(){
     stage.addChild(utils.cards.getNewCard());
   });
   addCardButton.x = 0.5 * gWidth - addCardButton.getBounds().width / 2;
   addCardButton.y = gHeight - addCardButton.getBounds().height / 2 - 20;
   stage.addChild(addCardButton);

   // Button to get back to the main menu
   var backButton = MTLG.utils.uiElements.addButton({
     text: locText("back"),
     sizeX: gWidth * 0.1,
     sizeY: 70,
     textColor: 'black',
     bgColor1: 'white',
     bgColor2: 'red'
   }, function(){

       infos.sharedInfos.master = null;
     // Leave the room and delete the sharedObject, so that no updates will be
     // received in the future.
     MTLG.distributedDisplays.rooms.leaveRoom(infos.room, function (result) {
         if (result && result.success) {
           MTLG.utils.inactivity.removeAllListeners();
           MTLG.distributedDisplays.actions.removeCustomFunction("requestSharedInfos");
           MTLG.distributedDisplays.actions.deregisterListenerForAction('updateSharedObject', updateSharedInfos);
           MTLG.distributedDisplays.actions.deregisterListenerForAction('addSharedObjects', addSharedObjectsListener);
           MTLG.distributedDisplays.SharedObject.getSharedObject(infos.sharedInfos.sharedId).delete();
           delete infos.sharedInfos;
           infos.initialized = false;
           MTLG.lc.goToMenu();
         } else {
             swal({
                 type: 'error',
                 title: locText("field1_leaveRoomErrorTitle"),
                 text: result.reason,
                 focusConfirm: true
             });
            infos.sharedInfos.master = MTLG.distributedDisplays.rooms.getClientId();
         }
     });

   });
   backButton.x =  gWidth - gWidth * 0.1;
   backButton.y = gHeight - 70;
   stage.addChild(backButton);

   // Add linkedAres for Players
   if(infos.sharedInfos.players.length > 0) {
     var x = 20;
     var y = gHeight / 4 - gHeight / 8;
     for(let i in infos.sharedInfos.players) {
       linkedAreas[infos.sharedInfos.players[i]] = utils.createLinkedArea(x, y, gWidth / 4, gHeight / 4, infos.sharedInfos.players[i], infos.sharedInfos.players[i]);
       x = gWidth - (gWidth/4) - 20;
     }
   }
 }


 /**
  * This function is called, when new sharedObjects were added. It gives the
  * new objects the ability to move
  *
  * @param  {type} {action action what happend
  * @param  {type} added   object with all sharedObjects that were added
  * @param  {type} from}    the sender of the objects
  */
 var addSharedObjectsListener = function({action, added, from}){
   var keys = Object.keys(added);
   // Add dragging to every object that is added
   if(keys.length >= 1) {
     var sharedId = keys[0];
     var object = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
     object.createJsObject.on('pressmove', utils.cards.moveCard);
   }
 }


 /**
  * This function updates the stage. If a player left the game, the corresponding
  * LinkedArea will be deleted.
  * If a player joinded the game, a linkedArea should be created.
  *
  * @param  {type} update the update object
  */
 var updateSharedInfos = function(update) {
   // If the change was a player, then a linked area should be deleted or created
   if ( update.propertyPath.startsWith('player')) {
     if(update.newValue != null) {
       // add a linkedArea
       var x = 20;
       var y = MTLG.getOptions().height / 4 - MTLG.getOptions().height / 8;

       var keys = Object.keys(linkedAreas);
       if (keys.length > 0) {
         if(linkedAreas[keys[0]].linkedArea.rect.x == 20) {
           x = MTLG.getOptions().width - (MTLG.getOptions().width/4) - 20;
         }
       }
       linkedAreas[update.newValue] = utils.createLinkedArea(x, y, MTLG.getOptions().width / 4, MTLG.getOptions().height / 4,  update.newValue,  update.newValue);
     } else {
       // delete LinkedArea
       utils.removeLinkedArea(linkedAreas[update.oldValue]);
     }
   }
 }



 module.exports = {
   check: checkGamefield,
   init: gamefield_init
 }
