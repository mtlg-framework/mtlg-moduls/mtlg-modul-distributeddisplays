/**
 * @Date:   2019-03-26T13:16:59+01:00
 * @Last modified time: 2019-05-15T11:48:36+02:00
 */
 try {
    var utils = require('./utils.js');
 } catch (error) {
   utils = utils || {};
 }
var swal = require('sweetalert2');

var infos = {};

// global variables
var roomList;
var locText = MTLG.lang.getString;
/**
 * Function that checks if this level should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLobby = function(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "masterSlaves") {
    return 1;
  }
  return 0;
}


/**
 * var lobby_init - This function initializes the lobby and calls the draw function
 *
 */
var lobby_init = function() {
  // refresh is a datastructure for a refresh function
  infos.refresh = {
    nextRefresh: Infinity,
    refreshDelta: 0,
    refreshFunction: null,
    refreshInterval: Infinity
  }
  // With this function a refresh function can be set.
  infos.setRefresh = function (refreshFunction, interval = 1000, next = null) {
    infos.refresh.refreshInterval = interval;
    infos.refresh.nextRefresh = next != null ? next : interval;
    infos.refresh.refreshFunction = refreshFunction;
  }
  infos.resetRefresh = function () {
    infos.refresh.refreshInterval = Infinity;
    infos.refresh.nextRefresh = infos.refresh.refreshInterval;
    infos.refresh.refreshFunction = null;
  }
  // Polling for refresh
  var handleTick = function(event) {
    if (!event.paused) { // Ticker is not paused
      infos.refresh.refreshDelta += event.delta;
      infos.refresh.nextRefresh -= event.delta;
      if (infos.refresh.nextRefresh <= 0) {
        infos.refresh.nextRefresh += infos.refresh.refreshInterval;
        if (infos.refresh.nextRefresh < 0) infos.refresh.nextRefresh = 0;
        if (infos.refresh.refreshFunction) infos.refresh.refreshFunction(infos.refresh.refreshDelta);
        infos.refresh.refreshDelta = 0;
      }
    }
  };
  createjs.Ticker.addEventListener("tick", handleTick);

  //for refreshing the list of available rooms every second
  infos.setRefresh(drawRoomSelection);

  draw();
}


/**
 * var draw - This function draws the lobby.
 *
 * @return {type}  description
 */
var draw = function() {
  var stage = MTLG.getStageContainer();
  var gWidth = MTLG.getOptions().width;
  var gHeight = MTLG.getOptions().height;
  // Container for the list of rooms
  roomList = new createjs.Container();

  // White Background Rectangle
  var background = new createjs.Shape();
  background.graphics.beginFill('white').drawRect(0, 0, gWidth, gHeight);
  coordinates = {
    xCoord: gWidth / 2,
    yCoord: gHeight / 2
  };
  background.regX = gWidth / 2;
  background.regY = gHeight / 2;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;

  // Button to add a room
  var addRoomButton = MTLG.utils.uiElements.addButton({
    text: locText("addRoom"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'black'
  }, function() {
    utils.rooms.addRoom(next);
  });
  addRoomButton.x = gWidth * 0.75;
  addRoomButton.y = gHeight * 0.25;

  // Button to refresh the available rooms
  var refreshRoomsButton = MTLG.utils.uiElements.addButton({
    text: locText("refreshRooms"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'black'
  }, function() {
    drawRoomSelection();
  });
  refreshRoomsButton.x = gWidth * 0.75;
  refreshRoomsButton.y = gHeight * 0.25 + 75;

  /// add all objects to the stageContainer
  stage.addChild(background);
  stage.addChild(roomList);
  stage.addChild(addRoomButton);
  stage.addChild(refreshRoomsButton);
}


/**
 * This function is a wrapper for the levelFInished function. Before leaving the
 * level the infos will be updated with the roomName and the refreshFunction
 * will be resetted.
 *
 * @param  {String} roomName Name of the chosen room
 */
var next = function(roomName) {
  // save the chosen room
  infos.room = roomName;
  // Reset refresh function
  infos.resetRefresh();

  // go to the next level
  MTLG.lc.levelFinished({
    nextLevel: "basicMasters.roleallocation",
    infos: infos
  });
};


/**
 * This function renders all available rooms and gives the possibility to join them.
 *  
 */
var drawRoomSelection = function() {
  var gWidth = MTLG.getOptions().width;
  var gHeight = MTLG.getOptions().height;
  utils.rooms.refreshRooms().then(function(rooms) {
    roomList.removeAllChildren();
    var x = gWidth * 0.2 / 2 + 5;
    var y = 35 + 5;
    for (let roomName in rooms) {
      var disabled = false;
      if (rooms[roomName].length >= 3) disabled = true;
      var roomButton = MTLG.utils.uiElements.addButton({
        text: roomName,
        sizeX: gWidth * 0.2,
        sizeY: 70,
        textColor: 'black',
        bgColor1: 'white',
        bgColor2: 'black'
      }, function() {
        if (disabled) {
          swal.fire({
            type: 'error',
            title: locText('Oops...'),
            text: locText('errorMoreThan3')
          });
        } else {
          MTLG.distributedDisplays.rooms.joinRoom(roomName, function() {
            next(roomName);
          });
        }
      });
      roomButton.x = x;
      roomButton.y = y;
      y += 75;
      roomList.addChild(roomButton);
    }
  });
};

module.exports = {
  check: checkLobby,
  init: lobby_init
};
