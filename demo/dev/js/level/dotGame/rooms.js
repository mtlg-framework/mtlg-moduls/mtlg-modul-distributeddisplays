/**
 * @Date:   2017-11-13T16:26:44+01:00
 * @Last modified time: 2019-05-14T11:13:52+02:00
 */


var locText = MTLG.lang.getString;
 var swal = require('sweetalert2');

 /**
  * Creates a new room after asking for its name
  */
 var addRoom = function (cb) {
     swal.fire({
         type: 'question',
         title: locText("field1_addRoomTitle"),
         text: locText("field1_addRoomQuestion"),
         input: 'text',
         inputPlaceholder: locText("field1_addRoomPlaceholder"),
         allowEnterKey: true,
         focusConfirm: false,
         showCancelButton: true,
         inputValidator: function (value) {
             return new Promise(function (resolve, reject) {
                 if (value) {
                     resolve();
                 } else {
                     reject(locText("field1_emptyInput"));
                 }
             });
         }
     }).then(function (name) {
       if(name.dismiss) {
         // swal cancelled, so do nothing
       } else {
         MTLG.distributedDisplays.rooms.createRoom(name.value, function (result) {
             if (result && result.success) {
                 cb(result.name);
             } else {
                 swal.fire({
                     type: 'error',
                     title: locText("field1_addRoomErrorTitle"),
                     text: result.reason,
                     focusConfirm: true
                 });
             }
         });
       }
     }, function (dismiss) {
         // User closed the dialog without adding a room
         // noop
     });
 };

 /**
  * Join an existing room
  * @param {object} event The click event
  * @param {string} name The name of the room to join
  */
 var joinRoomHandler = function (event, {name, cb}) {
     MTLG.distributedDisplays.rooms.joinRoom(name, function (result) {
         if (result && result.success) {
             cb(result.name);
         } else {
             swal.fire({
                 type: 'error',
                 title: locText("field1_joinRoomErrorTitle"),
                 text: result.reason,
                 focusConfirm: true
             });
         }
     });
 };

var rooms = {
  addRoom: addRoom,
  joinRoomHandler: joinRoomHandler
}
module.exports = {
  addRoom: addRoom,
  joinRoomHandler: joinRoomHandler
}
