/**
 * @Date:   2017-11-13T16:34:04+01:00
 * @Last modified time: 2019-05-14T10:56:04+02:00
 */

var swal = require('sweetalert2');
var locText = MTLG.lang.getString;
var demo = {};

 /**
  * Function that checks if this level should be the next level
  * @param the current game state (an object containing only nextLevel in this case)
  * @return a number in [0,1] that represents that possibility.
  */
 var check = function(gameState) {
   if (gameState && gameState.nextLevel && gameState.nextLevel == "dotGame.level3") {
     return 1;
   }
   return 0;
 }

/**
 * Initialize the third field: Game
 */
var feld3_init = function (gameState) {
    let stage = MTLG.getStageContainer();
    demo = gameState.demo;
    demo.field3.velocityX = 0;
    demo.field3.velocityY = 0;
    demo.field3.maxVelocity = 1 / 400;

    stage.removeAllChildren();

    drawField3();

    demo.refreshFunction = moveCharacter;
    demo.refreshInterval = 0;//ms
    demo.nextRefresh = demo.refreshInterval;

    if (demo.debug) {
        demo.keyMaster = false;
        demo.keyMap = {};
        var updateKeyMap = function (e) {
            e = e || event; // to deal with IE
            demo.keyMap[e.key] = e.type === "keydown";
            if (e.type === "keydown") demo.keyMaster = true;
        }
        window.addEventListener("keydown", updateKeyMap);
        window.addEventListener("keyup", updateKeyMap);
    }
};

/**
 * Draw the third field: Game
 */
var drawField3 = function () {
    let stage = MTLG.getStageContainer();
    var w = demo.w;
    var h = demo.h;


    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill(demo.obstacleColor).drawRect(0, 0, 100* w, 100*h);

    /**
     * Switches to the previous field: Control selection
     */
    var backToSelectControls = function () {
        demo.refreshFunction = null;
        demo.refreshInterval = Infinity;
        demo.field3 = {};
        MTLG.lc.levelFinished({
          nextLevel: "dotGame.level2",
          demo: demo
        });
    };

    // Back to control selection
    var backToSelectControlsButton = MTLG.utils.uiElements.addButton({
      text: locText("field3_backToSelectControlsButton"),
      sizeX: w * 10,
      sizeY: h * 5,
      font: demo.font,
      textColor: demo.textColor,
      bgColor1: demo.buttonBackgroundColor,
      bgColor2: "black",
      radius: 5 * h / 10
    }, backToSelectControls);
    backToSelectControlsButton.x = w + backToSelectControlsButton.getBounds().width / 2;
    backToSelectControlsButton.y = 50 * h;



    // Scaling

    // Container for scaling, children can be attached with absolute positon and scale, they are scaled and positioned by this parent without affecting their properties
    var scaledContainer = new createjs.Container();
    scaledContainer.x = 50 * w;
    scaledContainer.y = 50 * h;
    scaledContainer.scaleX = w;
    scaledContainer.scaleY = w;


    // Control

    // Container
    demo.field3.controlContainer = new createjs.Container();
    demo.field3.controlContainer.x = 35;
    demo.field3.controlContainer.y = 28 * h / w;

    // Control element
    var controlBackground = new createjs.Shape();
    demo.field3.controlButton = new createjs.Shape();
    switch (demo.controlling) {
        case "x":
            controlBackground.graphics.beginFill(demo.transparentColor).drawRect(-10, -2, 20, 4);
            controlBackground.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(-8, -0.5, 16, 1, 0.5).drawRect(-0.1, -1, 0.2, 2);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(0, 0, 1);
            // Controlled by x
            demo.field3.controlContainer.y = 35 * h / w;
            break;
        case "y":
            controlBackground.graphics.beginFill(demo.transparentColor).drawRect(-2, -10, 4, 20);
            controlBackground.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(-0.5, -8, 1, 16, 0.5).drawRect(-1, -0.1, 2, 0.2);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(0, 0, 1);
            // Controlled by y
            demo.field3.controlContainer.x = 43;
            break;
        case "rotation":
            controlBackground.graphics.beginFill(demo.transparentColor).drawCircle(0, 0, 10);
            controlBackground.graphics.setStrokeStyle(1).beginStroke(demo.buttonBackgroundColor).drawCircle(0, 0, 6);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(6, 0, 1.5);
            // Controlled by rotation
            if (demo.sharedRoom.character) demo.field3.controlButton.rotation = demo.sharedRoom.character.rotation;
            break;
        case "size":
            controlBackground.graphics.beginFill(demo.transparentColor).drawRect(-10, -2, 20, 4);
            controlBackground.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(-8, -0.5, 16, 1, 0.5);
            demo.field3.controlButton.graphics.beginFill(demo.selectedColor).drawCircle(0, 0, 1);
            // Controlled by x
            demo.field3.controlContainer.y = 35 * h / w;
            if (demo.sharedRoom.character) demo.field3.controlButton.x = ((demo.sharedRoom.character.children[1].x - 3) * 2) - 8;
            break;
        default:
            console.warn("drawField3: No drawing defined for control element", demo.controlling);
            break;
    }
    demo.field3.controlContainer.addChild(controlBackground);
    demo.field3.controlContainer.addChild(demo.field3.controlButton);
    demo.field3.controlContainer.on("mousedown", controlMovedHandler);
    demo.field3.controlContainer.on("pressmove", controlMovedHandler);
    demo.field3.controlContainer.on("pressup", controlMovedHandler);


    // Game

    // Container to compensate the character's movement
    demo.field3.offsetContainer = new createjs.Container();

    // Draw the field of the game
    var levelContainer = drawGameField();
    demo.field3.offsetContainer.addChild(levelContainer);

    // Add the synchronized character
    demo.field3.offsetContainer.addChild(demo.sharedRoom.character);

    scaledContainer.addChild(demo.field3.offsetContainer);
    scaledContainer.addChild(demo.field3.controlContainer);

    stage.addChild(background);
    stage.addChild(scaledContainer);
    stage.addChild(backToSelectControlsButton);
    drawFullscreenButton();
};

/**
 * Draws the game field into a new Container
 * @returns {Container} The created Container for the game field
 */
var drawGameField = function () {
    var level = new createjs.Container();
    demo.field3.obstacles = [];

    // First all containers
    level.addChild(createObstacleCircle(true, 0, 0, 25)); // O Big start circle
    level.addChild(createObstacleRectangle(true, -5, 20, 10, 30, 0)); // | | Vertical
    level.addChild(createObstacleRectangle(true, -2, 40,  4, 40, 0)); // || Vertical
    level.addChild(createObstacleCircle(true, 0, 80, 5)); // o Small joint circle
    level.addChild(createObstacleRectangle(true,   0,    78.6,  30,  2.8, 0)); // - Small horizontal
    level.addChild(createObstacleRectangle(true,  20,    70,    60, 50,   0)); // ___| Big area
    level.addChild(createObstacleRectangle(true,  60,   107,   200, 15,  22)); // \\ Long diagonal tunnel's first part
    level.addChild(createObstacleRectangle(true, 245.4, 181.9, 125, 15,  39)); // \\ Long diagonal tunnel's second part
    level.addChild(createObstacleCircle(true, 400, 183, 100)); // O Giant arc
    level.addChild(createObstacleCircle(true, 300,  80,  50)); // O Medium arc
    level.addChild(createObstacleCircle(true, 260,  50,  25)); // C Bumper
    level.addChild(createObstacleCircle(true, 300,  20,  12.5)); // O Min arc to cycle around

    level.addChild(createObstacleCircle(true, 240, 190.5, 10)); // Patching a float calculation error at the tunnel's joint
    level.addChild(createObstacleRectangle(true, 325, 233, 30, 20,  39)); // Patching a float calculatoin error at the tunnel - Giant Arc joint
    level.addChild(createObstacleRectangle(true, 315, 125, 41, 15, -45)); // Patching a float calculatoin error at the Medium - Giant Arc joint
    level.addChild(createObstacleRectangle(true, 293,  29, 14,  4,   0)); // Patching a float calculatoin error at the cycle around circle joint

    // Then all other obstacles
    level.addChild(createObstacleRectangle(false, -2.2, 50,   -10,  10,  60)); // Slope for the smaller vertical left
    level.addChild(createObstacleRectangle(false,  2.2, 50,    10,  10, -60)); // Slope for the smaller vertical right
    level.addChild(createObstacleRectangle(false,  20,  81.7,   5,   5,  30)); // Entry slope for the Big area
    level.addChild(createObstacleRectangle(false,  20,  73.5,  10, -10, -45)); // Upper left clipping of the Big area
    level.addChild(createObstacleRectangle(false,  15,  84,    12,  40,   0)); // Shrink the lower left side of the Big area
    level.addChild(createObstacleRectangle(false,  30,  60,     3,  34,   0)); // | Vertical bar
    level.addChild(createObstacleRectangle(false,  30,  98,   30.75, 7,   0)); // -- Middle bar
    level.addChild(createObstacleRectangle(false,  25, 108,   200,  20,  19)); // \ Slope below the Middle bar, then lower bound of Long diagonal tunnel's first part
    level.addChild(createObstacleRectangle(false,  55,  88,     8,  20,  60)); // ^ Peak ontop of the Middle bar 1
    level.addChild(createObstacleRectangle(false,  55,  88,    11.5, 8,  60)); // ^ Peak ontop of the Middle bar 2
    level.addChild(createObstacleRectangle(false,  55,  70,    50, -25,  60)); // \ Slope in the upper right corner of the Big area
    level.addChild(createObstacleRectangle(false, 214, 173.1, 200,  20,  35)); // \ Lower bound of Long diagonal tunnel's second part
    level.addChild(createObstacleCircle(false, 400, 183, 90)); // 0 Inner Giant arc
    level.addChild(createObstacleCircle(false, 300,  79, 45)); // 0 Inner Medium arc
    level.addChild(createObstacleCircle(false, 260,  50, 20)); // c Inner Bumper
    level.addChild(createObstacleCircle(false, 300,  20,  9.5)); // 0 Inner Min arc to cycle around

    return level;
};

/**
 * Creates an obstacle with the shape of a circle and adds it to the demo.field3.obstacles Array
 * @param {boolean} isContainer Has the character to be inside or outside of the obstacle
 * @param {number} x The x-coordinate of the circle
 * @param {number} y The y-coordinate of the circle
 * @param {number} r The radius of the circle
 * @returns {Shape} The created obstacle
 */
var createObstacleCircle = function (isContainer, x, y, r) {
    var circ = new createjs.Shape();
    circ.graphics.beginFill(isContainer ? demo.noObstacleColor : demo.obstacleColor).drawCircle(0, 0, r);
    circ.isContainer = isContainer;
    circ.x = x;
    circ.y = y;
    circ.type = "circle";
    circ.r = r;

    // Store precomputed values
    if (isContainer) {
        circ.r2 = (r - 1) * (r - 1);
    } else {
        circ.r2 = (r + 1) * (r + 1);
    }

    demo.field3.obstacles.push(circ);
    return circ;
};


/**
 * Creates an obstacle with the shape of a rectangle and adds it to the demo.field3.obstacles Array
 * @param {boolean} isContainer Has the character to be inside or outside of the obstacle
 * @param {number} x The x-coordinate of the rectangle
 * @param {number} y The y-coordinate of the rectangle
 * @param {number} w The width of the rectangle
 * @param {number} h The height of the rectangle
 * @param {number} [rotation = 0] The rotation of the rectangle
 * @returns {Shape} The created obstacle
 */
var createObstacleRectangle = function (isContainer, x, y, w, h, rotation) {
    var rect = new createjs.Shape();
    rect.graphics.beginFill(isContainer ? demo.noObstacleColor : demo.obstacleColor).drawRect(0, 0, w, h);
    rect.isContainer = isContainer;
    rect.x = x;
    rect.y = y;
    rect.rotation = rotation || 0;
    rect.type = "rectangle";

    // Store precomputed values
    rect.rad = rect.rotation * degToRad;
    if (isContainer) {
        // Shrink the container to a smaller rectangle with a margin of the character's dot's radius
        rect.ax = rect.x + Math.cos((rect.rotation + 45) * degToRad) * sqrt2; // coordinates of the A corner
        rect.ay = rect.y + Math.sin((rect.rotation + 45) * degToRad) * sqrt2;
        rect.ab = [Math.cos(rect.rad) * (w - 2), Math.sin(rect.rad) * (w - 2)]; // Vectors in Rectangle A B
        rect.ad = [-Math.sin(rect.rad) * (h - 2), Math.cos(rect.rad) * (h - 2)]; //                      D C
    } else {
        // Extend the container to a bigger rectangle with a padding of the character's dot's radius
        rect.ax = rect.x - Math.cos((rect.rotation + 45) * degToRad) * sqrt2; // coordinates of the A corner
        rect.ay = rect.y - Math.sin((rect.rotation + 45) * degToRad) * sqrt2;
        rect.ab = [Math.cos(rect.rad) * (w + 2), Math.sin(rect.rad) * (w + 2)]; // Vectors in Rectangle A B
        rect.ad = [-Math.sin(rect.rad) * (h + 2), Math.cos(rect.rad) * (h + 2)]; //                      D C
    }
    rect.abab = dot(rect.ab, rect.ab); // Dot product of the two vectors
    rect.adad = dot(rect.ad, rect.ad);

    demo.field3.obstacles.push(rect);
    return rect;
};


/**
 * Draws a button to toggle fullscreen
 */
var drawFullscreenButton = function () {
    let stage = MTLG.getStageContainer();
    var button = MTLG.utils.uiElements.addButton({
      text: locText("general_fullscreenButton"),
      sizeX: 10 * demo.w,
      sizeY: 3 * demo.h,
      font: demo.font,
      textColor: demo.textColor,
      bgColor1: demo.buttonBackgroundColor,
      bgColor2: "black",
      radius: 3 * demo.h / 10
    }, toggleFullscreen);
    button.x = demo.h + button.getBounds().width / 2;
    button.y = demo.h + button.getBounds().height / 2;
    stage.addChild(button);
};

/**
 * Toggles fullscreen
 */
var toggleFullscreen = function () {
    var isFullscreen = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
    var elem = document.body;
    if (!isFullscreen) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
};


var degToRad = Math.PI / 180;
var radToDeg = 180 / Math.PI;
var sqrt2 = Math.sqrt(2);

/**
 * Returns a number whose value is limited to the given range.
 * @param {number} value The number to clamp
 * @param {number} min The lower boundary of the output range
 * @param {number} max The upper boundary of the output range
 * @returns {number} The clamped number in the range [min, max]
 */
var clamp = function (value, min, max) {
    return Math.min(Math.max(value, min), max);
};

/**
 * Creates a new button like element, inline text button
 * @param {number} h The height of the button, its width is calculated from the text width
 * @param {string} text The text to display on the button
 * @returns {Container} The created button element
 */
var createButton = function (h, text) {
    var buttonContainer = new createjs.Container();
    var textObj = new createjs.Text(text, demo.buttonFont, demo.textColor);
    textObj.textBaseline = 'middle';
    textObj.textAlign = 'center';
    var bounds = textObj.getBounds();
    textObj.x = (bounds.width + h) / 2; // Width of text + padding
    textObj.y = h / 2;

    var back = new createjs.Shape();
    back.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(0, 0, bounds.width + h, h, h / 10);

    buttonContainer.addChild(back);
    buttonContainer.addChild(textObj); // Text should be the second child
    return buttonContainer;
};


/**
 * Handler for the movement of the control button
 * @param {object} event The click event
 */
var controlMovedHandler = function (event) {
    if (!demo.sharedRoom.gameOver) {
        if (event.type === "pressup") {
            // Snap back to 0 on pressup
            switch (demo.controlling) {
                case "x":
                    demo.field3.controlButton.x = 0;
                    demo.field3.velocityX = 0;
                    break;
                case "y":
                    demo.field3.controlButton.y = 0;
                    demo.field3.velocityY = 0;
                    break;
            }
        } else {
            var relativePoint = demo.field3.controlContainer.globalToLocal(event.stageX, event.stageY);
            switch (demo.controlling) {
                case "x":
                    var newX = clamp(relativePoint.x, -8, 8); // Boundary of the slider
                    demo.field3.controlButton.x = newX;
                    demo.field3.velocityX = newX * demo.field3.maxVelocity;
                    break;
                case "y":
                    var newY = clamp(relativePoint.y, -8, 8); // Boundary of the slider
                    demo.field3.controlButton.y = newY;
                    demo.field3.velocityY = newY * demo.field3.maxVelocity;
                    break;
                case "rotation":
                    var newRotation = Math.atan2(relativePoint.y, relativePoint.x) * radToDeg; // Convert to angle in radians and then to degree, mind atan2(y, x) not x,y
                    demo.field3.controlButton.rotation = newRotation;
                    demo.sharedRoom.character.rotation = newRotation;
                    break;
                case "size":
                    var newSize = clamp(relativePoint.x, -8, 8); // Boundary of the slider
                    demo.field3.controlButton.x = newSize;
                    newSize = ((newSize + 8) / 2) + 3; // Transform: [-8,8], [0,16], [0,8], [3,11] the outer size is then in [8, 24]
                    demo.sharedRoom.character.children[0].x = -newSize;
                    demo.sharedRoom.character.children[1].x = newSize;
                    break;
            }
        }
    }
};

/**
 * Moves the character according to the current velocity
 * @param {number} delta Time since last frame
 */
var moveCharacter = function (delta) {
    if (demo.debug && demo.keyMaster) {
        var velX = 0,
            velY = 0,
            rot = demo.sharedRoom.character.rotation,
            size = demo.sharedRoom.character.children[1].x;

        if (demo.keyMap.ArrowRight) velX += 0.2;
        if (demo.keyMap.ArrowLeft) velX -= 0.2;
        if (velX !== 0) demo.sharedRoom.character.x += velX;

        if (demo.keyMap.ArrowUp) velY -= 0.2;
        if (demo.keyMap.ArrowDown) velY += 0.2;
        if (velY !== 0) demo.sharedRoom.character.y += velY;

        if (demo.keyMap.a) rot -= 2;
        if (demo.keyMap.d) rot += 2;
        if (rot !== demo.sharedRoom.character.rotation) demo.sharedRoom.character.rotation = rot;

        if (demo.keyMap.w) {
            if (size <= 10.8) {
                size += 0.2;
            } else {
                size = 11;
            }
            demo.sharedRoom.character.children[0].x = -size;
            demo.sharedRoom.character.children[1].x = size;
        }
        if (demo.keyMap.s) {
            if (size >= 3.2) {
                size -= 0.2;
            } else {
                size = 3;
            }
            demo.sharedRoom.character.children[0].x = -size;
            demo.sharedRoom.character.children[1].x = size;
        }

        // Update remote control elements
        demo.sharedRoom.debug.velocityX = velX * 20;
        demo.sharedRoom.debug.velocityY = velY * 20;
        demo.sharedRoom.debug.rotation = rot;
        demo.sharedRoom.debug.size = ((size - 3) * 2) - 8;

        // Update own control element
        if (demo.controlling && demo.field3.controlButton) {
            if (demo.controlling === "x") demo.field3.controlButton.x = velX * 20;
            if (demo.controlling === "y") demo.field3.controlButton.y = velY * 20;
            if (demo.controlling === "rotation") demo.field3.controlButton.rotation = rot;
            if (demo.controlling === "size") demo.field3.controlButton.x = ((size - 3) * 2) - 8;
        }
    }

    if (demo.controlling === "x" && demo.field3.velocityX !== 0) demo.sharedRoom.character.x += demo.field3.velocityX * delta;
    if (demo.controlling === "y" && demo.field3.velocityY !== 0) demo.sharedRoom.character.y += demo.field3.velocityY * delta;
    // Move the offset container
    updateOffsetContainer(delta);
    checkGameOver();
};

/**
 * Update the offset to compensate the characters movement
 * @param {number} delta Time since last frame
 */
var updateOffsetContainer = function (delta) {
    if (demo.field3.offsetContainer) {
        var diffX = demo.field3.offsetContainer.x + demo.sharedRoom.character.x;
        var diffY = demo.field3.offsetContainer.y + demo.sharedRoom.character.y;
        // If more than 20 percent off of the center, update the offsetContainer: difference above 20, from which a twentyfith is compensated each frame
        if (Math.abs(diffX) > 10) demo.field3.offsetContainer.x -= (diffX - (Math.sign(diffX) * 10)) / 25;
        if (Math.abs(diffY) > 10) demo.field3.offsetContainer.y -= (diffY - (Math.sign(diffY) * 10)) / 25;
    }
};

/**
 * Checks if the character has collided with something
 */
var checkGameOver = function () {
    // Positions of the two points
    var dx = Math.cos(demo.sharedRoom.character.rotation * degToRad) * demo.sharedRoom.character.children[1].x; // Positive child
    var dy = Math.sin(demo.sharedRoom.character.rotation * degToRad) * demo.sharedRoom.character.children[1].x; // Positive child
    var x1 = demo.sharedRoom.character.x + dx;
    var x2 = demo.sharedRoom.character.x - dx;
    var y1 = demo.sharedRoom.character.y + dy;
    var y2 = demo.sharedRoom.character.y - dy;

    // Check if the character is indide of any container
    var inContainer1 = false;
    var inContainer2 = false;
    var i = 0;
    while (i < demo.field3.obstacles.length && demo.field3.obstacles[i].isContainer && !(inContainer1 && inContainer2)) {
        if (!inContainer1 && checkDotInObstacle(x1, y1, demo.field3.obstacles[i])) {
            inContainer1 = true;
        }
        if (!inContainer2 && checkDotInObstacle(x2, y2, demo.field3.obstacles[i])) {
            inContainer2 = true;
        }
        i++;
    }
    if (!(inContainer1 && inContainer2)) return gameOver();

    // Check if touching any not container obstacle
    while (i < demo.field3.obstacles.length) {
        if (!demo.field3.obstacles[i].isContainer && (checkDotInObstacle(x1, y1, demo.field3.obstacles[i]) || checkDotInObstacle(x2, y2, demo.field3.obstacles[i]))) {
            return gameOver();
        }
        i++;
    }
    demo.sharedRoom.gameOver = false;
};


/**
 * Lost the game
 */
var gameOver = function () {
    if (!demo.sharedRoom.gameOver) {
        demo.sharedRoom.gameOver = true;
        demo.field3.velocityX = 0;
        demo.field3.velocityY = 0;
        swal.fire({
            type: 'error',
            title: locText("field3_gameOverTitle"),
            text: locText("field3_gameOverText"),
            confirmButtonText: locText("field3_gameOverConfirm"),
            allowEnterKey: true,
            allowEscapeKey: demo.debug === true, // Keep going with escape key
            allowOutsideClick: false,
            showCancelButton: false
        }).then(function () {
            // Confirmed to restart
            demo.sharedRoom.character.x = 0;
            demo.sharedRoom.character.y = 0;
            demo.sharedRoom.character.rotation = 0;
            demo.sharedRoom.character.children[0].x = -7;
            demo.sharedRoom.character.children[1].x = 7;
            demo.sharedRoom.gameOver = false;
            if (demo.field3.controlButton) {
                demo.field3.controlButton.x = 0;
                demo.field3.controlButton.y = 0;
                demo.field3.controlButton.rotation = 0;
            }
        }, function (dismiss) {
            // Dismissed the popup, keep playing, only possible if debug === true
            // noop
        });
    }
};



/**
 * If the obstacle is a  container, it returns whether the character's dot is completely inside the given obstacle
 * If the obstacle is no container, it returns whether the character's dot touches the given obstacle
 * @param {number} x The x-coordinate of the dot
 * @param {number} y The y-coordinate of the dot
 * @param {Shape} obstacle The obstacle container to check
 * @returns {boolean} True, if the dot is completely inside the container or touches the obstacle
 */
var checkDotInObstacle = function (x, y, obstacle) {
    switch (obstacle.type) {
        case "circle":
            return ((obstacle.x - x) * (obstacle.x - x)) + ((obstacle.y - y) * (obstacle.y - y)) < obstacle.r2;
            break;
        case "rectangle":
            var apab = dot([x - obstacle.ax, y - obstacle.ay], obstacle.ab);
            var apad = dot([x - obstacle.ax, y - obstacle.ay], obstacle.ad);
            return 0 < apab && apab < obstacle.abab && 0 < apad && apad < obstacle.adad;
            break;
        default:
            console.warn("checkDotInObstacle: No check for the obstacle type", obstacle.type, "defined.");
            break;
    }
};


/**
 * Calculates the dot product between the two vectors (Arrays) a and b
 * @param {Array.<number>} a The first vector of the dot product
 * @param {Array.<number>} b The second vector of the dot product
 * @returns {number} The dot product between the two vectors (Arrays) a and b
 */
var dot = function (a, b) {
    return (a[0] * b[0]) + (a[1] * b[1]);
};

module.exports = {
  init: feld3_init,
  check: check
}
