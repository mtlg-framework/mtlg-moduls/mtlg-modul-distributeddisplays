/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2019-05-15T08:51:41+02:00
 */
var locText = MTLG.lang.getString;
var rooms;
try {
   rooms = require('./rooms.js');
} catch (error) {
  rooms = rooms;
}



var swal = require('sweetalert2');
/**
 * Function that checks if Feld01 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel1 = function(gameState){
  if (gameState && gameState.nextLevel && gameState.nextLevel == "dotGame") {
    return 1;
  }
  return 0;
}

/**
 * General storage object for the demo
 * Is not global, but accessible by all scripts
 */
var demo = {
  field1: {}, // Relevant for a single field, cleared after leaving the field
  field2: {},
  field3: {},
  nextRefresh: Infinity,
  refreshDelta: 0,
  refreshFunction: null,
  refreshInterval: Infinity,
  roomName: "",
  sharedRoom: null // Room object shared between all members
};

// Polling for refresh
demo.handleTick = function (event) {
  if (!event.paused) { // Ticker is not paused
      demo.refreshDelta += event.delta;
      demo.nextRefresh -= event.delta;
      if (demo.nextRefresh <= 0) {
          demo.nextRefresh += demo.refreshInterval;
          if (demo.nextRefresh < 0) demo.nextRefresh = 0;
          if (demo.refreshFunction) demo.refreshFunction(demo.refreshDelta);
          demo.refreshDelta = 0;
      }
  }
};
createjs.Ticker.addEventListener("tick", demo.handleTick);

/*
* manages drawing into playing fields of every area
*/

/**
 * Initialize the first field: Room selection
 */
var feld1_init = function () {
    let stage = MTLG.getStageContainer();

    if (!demo.initialized) {
        // Setup the customActions to request the sharedRoom object
        MTLG.distributedDisplays.actions.setCustomFunction("getSharedRoom", customActionGetSharedRoom);
        MTLG.distributedDisplays.actions.setCustomFunction("receiveSharedRoom", customActionReceiveSharedRoom);
        // Setup customFunction to react to updates
        MTLG.distributedDisplays.actions.registerListenerForAction("updateSharedObject", customFunctionOnUpdate);
        demo.debug = true;
        demo.initialized = true;
    }

    stage.removeAllChildren();
    demo.roomName = "";
    demo.sharedRoomObject = null;
    demo.refreshFunction = refreshRoomList;
    demo.refreshInterval = 1000;//ms
    demo.nextRefresh = demo.refreshInterval;
    demo.w = MTLG.getOptions().width / 100.0;
    demo.h = MTLG.getOptions().height / 100.0;
    demo.font = "Helvetica";
    demo.textColor = "#333";
    demo.disabledColor = "#DDD";
    demo.disabledBackgroundColor = "#EEE";
    demo.noObstacleColor = "#F3F8FE"; // Almost white
    demo.buttonBackgroundColor = "#9DC2F6"; // Light
    demo.characterColor = "#619EF3"; // Base
    demo.selectedColor = "#2B7FF2"; // Slightly darker
    // dark="#0367F0"
    demo.obstacleColor = "#162437"; // Other even darker
    demo.transparentColor = "rgba(255, 255, 255, 0.01)";
    demo.titleFont = Math.ceil(3 * demo.h) + "px " + demo.font;
    demo.normalFont = Math.ceil(2 * demo.h) + "px " + demo.font;
    demo.buttonFont = "bold " + demo.normalFont;
    demo.buttonMargin = 30 + demo.w;//px
    drawField1(MTLG.getOptions().width, MTLG.getOptions().height);
};

/**
 * Draw the first field: Room selection
 */
var drawField1 = function () {
    let stage = MTLG.getStageContainer();

    var w = MTLG.getOptions().width;
    var h = MTLG.getOptions().height;

    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill("#FFF").drawRect(0, 0, w, h);

    // Text
    var title = new createjs.Text(locText("field1_title"), demo.titleFont, demo.textColor);
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 0.5 * w;
    title.y = 0.1 * h;

    // Rooms

    // Room selection wrapper
    var roomSelection = new createjs.Container();
    roomSelection.x = 0.25 * w;
    roomSelection.y = 0.15 * h;

    // Refresh rooms button
    var refreshRoomsButton = MTLG.utils.uiElements.addButton({
      text: locText("field1_refreshButton"),
      sizeX: w * 0.1,
      sizeY: h * 0.05,
      font: demo.font,
      textColor: demo.textColor,
      bgColor1: demo.buttonBackgroundColor,
      bgColor2: "black",
      radius: h * 0.05 / 10
    }, refreshRoomList);


    // Create new room button
    var newRoomButton = MTLG.utils.uiElements.addButton({
      text: locText("field1_newRoomButton"),
      sizeX: w * 0.1,
      sizeY: h * 0.05,
      font: demo.font,
      textColor: demo.textColor,
      bgColor1: demo.buttonBackgroundColor,
      bgColor2: "black",
      radius: h * 0.05 / 10
    }, function () {
      rooms.addRoom(roomSelected);
    });
    newRoomButton.x = refreshRoomsButton.getBounds().width + demo.buttonMargin;

    // Room list
    demo.field1.roomList = new createjs.Container();
    demo.field1.roomList.y = 0.6 * h;

    roomSelection.addChild(refreshRoomsButton);
    roomSelection.addChild(newRoomButton);
    roomSelection.addChild(demo.field1.roomList);

    stage.addChild(background);
    stage.addChild(title);
    stage.addChild(roomSelection);
    //drawFullscreenButton();
};





/**
 * Refreshes the list of existing rooms
 */
var refreshRoomList = function () {
    MTLG.distributedDisplays.rooms.getAllRooms(function (result) {
        if (result) {
            demo.field1.roomList.removeAllChildren();
            var x, y = 0;
            var w = MTLG.getOptions().width;
            var h = MTLG.getOptions().height;
            for (let roomName in result) {
                var button = MTLG.utils.uiElements.addButton({
                  text: locText("field1_joinRoomButton"),
                  sizeX: w * 0.1,
                  sizeY: h * 0.03,
                  font: demo.font,
                  textColor: demo.textColor,
                  bgColor1: demo.buttonBackgroundColor,
                  bgColor2: "black",
                  radius: h * 0.03 / 10
                }, function (event) {
                  rooms.joinRoomHandler(event, {'name': roomName, 'cb': roomSelected})
                });
                button.y = y;

                if (!x) x = button.getBounds().width + demo.buttonMargin;

                var text = new createjs.Text(roomName, demo.normalFont, demo.textColor);
                text.textBaseline = 'middle'; // Align with button
                text.x = x;
                text.y = y + 1.5 * demo.h; // Align with button
                text.regX = button.getBounds().width / 2;
                text.regY = button.getBounds().height / 2;

                demo.field1.roomList.addChild(button);
                demo.field1.roomList.addChild(text);
                y += 5 * demo.h;
            }
        }
    });
};

/**
 * Switches to the next field: Control selection
 * @param {string} roomName The name of the joined or created room
 */
var roomSelected = function (roomName) {
    demo.roomName = roomName;
    demo.refreshFunction = null;
    demo.field1 = {};

    MTLG.lc.levelFinished({
      nextLevel: "dotGame.level2",
      demo: demo
    });
};





/**
 * This customAction can be called to request the sharedRoom object from other room members
 * @param {string} requester The identifier of the client that requested the sharedRoom
 * @param {string} roomName The name of the sharedRoom's room
 */
var customActionGetSharedRoom = function (requester, roomName) {
    if (demo.sharedRoom && demo.roomName === roomName) { // Has a sharedRoom itself and the requester is a member of this room
        var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(demo.sharedRoom.sharedId);
        MTLG.distributedDisplays.communication.sendCustomAction(requester, "receiveSharedRoom", sharedObj.toSendables());
    }
};

/**
 * This customAction is called as a response to customActionGetSharedRoom and transmits the sharedRoom to the requester
 * @param {Sendable} sendableRoom The sendable representation of the sharedRoom
 */
var customActionReceiveSharedRoom = function (sendableRoom) {
    if (!demo.sharedRoom) { // Not already received from a different member
        // Get the sharedId from the associative list
        var sharedId;
        for (var i in sendableRoom) {
            if (sendableRoom[i].sharedId) {
                sharedId = sendableRoom[i].sharedId;
                break;
            }
        }

        // Convert the sharedRoom from its sendable and assign the wrapped object to demo.sharedRoom
        // The addSharedObjects action is used instead of SharedObject.fromSendable, to support more complex sendableRoom structures in the future
        MTLG.distributedDisplays.actions.addSharedObjects(sendableRoom);
        var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
        demo.sharedRoom = sharedObj.createJsObject;

        // Update start / join button
        if (demo.sharedRoom.inGame) {
            if (demo.field2.startButton) demo.field2.startButton.children[1].text = locText("field2_joinGameButton");
            if (demo.field2.joinGameNote) demo.field2.joinGameNote.visible = true;
        }
    }
};

/**
 * This custom function is called after receiving an update for a SharedObject, reacts to this
 * @param {object} action The executed action
 */
var customFunctionOnUpdate = function (action) {
    if (demo.sharedRoom && demo.sharedRoom.sharedId === action.sharedId) { // SharedRoom object has changed
        switch (action.propertyPath) {
            case "inGame":
                if (action.newValue && !action.oldValue) { // If a member started the game
                    if (demo.controlling) {
                        startGame(); // If a control is selected, start as well
                    } else {
                        // If no control is selected, update start / join button
                        if (demo.field2.startButton) demo.field2.startButton.children[1].text = locText("field2_joinGameButton");
                        if (demo.field2.joinGameNote) demo.field2.joinGameNote.visible = true;
                    }
                } else if (!action.newValue && action.oldValue) {
                    if (demo.field2.startButton) demo.field2.startButton.children[1].text = locText("field2_startButton");
                    if (demo.field2.joinGameNote) demo.field2.joinGameNote.visible = false;
                }
                break;

            case "debug.velocityX":
                if (demo.controlling === "x" && demo.field3.controlButton) demo.field3.controlButton.x = action.newValue;
                break;
            case "debug.velocityY":
                if (demo.controlling === "y" && demo.field3.controlButton) demo.field3.controlButton.y = action.newValue;
                break;
            case "debug.rotation":
                if (demo.controlling === "rotation" && demo.field3.controlButton) demo.field3.controlButton.rotation = action.newValue;
                break;
            case "debug.size":
                if (demo.controlling === "size" && demo.field3.controlButton) demo.field3.controlButton.x = action.newValue;
                break;
        }
    }
};

/**
 * Switches to the next field: Game
 */
var startGame = function () {
    if (demo.controlling) {
        demo.refreshFunction = null;
        demo.refreshInterval = Infinity;
        demo.field2 = {};
        demo.sharedRoom.inGame = true;
        MTLG.lc.levelFinished({
          nextLevel: "dotGame.level3",
          demo: demo
        });
    }
};

/**
 * Creates a new button like element, inline text button
 * @param {number} h The height of the button, its width is calculated from the text width
 * @param {string} text The text to display on the button
 * @returns {Container} The created button element
 */
var createButton = function (h, text) {
    var buttonContainer = new createjs.Container();
    var textObj = new createjs.Text(text, demo.buttonFont, demo.textColor);
    textObj.textBaseline = 'middle';
    textObj.textAlign = 'center';
    var bounds = textObj.getBounds();
    textObj.x = (bounds.width + h) / 2; // Width of text + padding
    textObj.y = h / 2;

    var back = new createjs.Shape();
    back.graphics.beginFill(demo.buttonBackgroundColor).drawRoundRect(0, 0, bounds.width + h, h, h / 10);

    buttonContainer.addChild(back);
    buttonContainer.addChild(textObj); // Text should be the second child
    return buttonContainer;
};

module.exports = {
  init: feld1_init,
  check: checkLevel1
}
