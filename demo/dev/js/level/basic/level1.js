/**
 * @Date:   2019-03-19T10:14:00+01:00
 * @Last modified time: 2019-08-14T11:19:18+02:00
 */

// for easier use
var locText = MTLG.lang.getString;

/**
 * Function that checks if level1 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel1 = function(gameState){
  if (gameState && gameState.nextLevel && gameState.nextLevel == "basic") {
    return 1;
  }
  return 0;
}

/*
* This variable is a global variable in this file, because this variable is
* for holding the sharedObject. The ListenerFunctions must know this and
* therefore it is global.
*/
var blueCircleObject;

/**
 * Initialize the first field: Room selection
 */
var level1_init = function () {
    // try to create room. If the room will be created, this was the first
    // attempt to create it and therefore this client is the master. If it is
    // not possible to create the room, then the room is already created and
    // there is already a master, therefore this client is a slave.
    MTLG.distributedDisplays.rooms.createRoom('room', function(result){
      if(result && result.success){
        // This client is a master and therefore passes true to the draw function
        drawField1(true);
      }else{
        // This client is a slave. Now it tries to join the room
        MTLG.distributedDisplays.rooms.joinRoom('room', function(result) {
          if(result && result.success){
            // if joined, the field should be drawn with false (for not master)
            // as parameter
            drawField1(false);
          }
        });
      }
    });
};

/**
 * Draw the first field: Room selection
 */
var drawField1 = function (master) {
    let stage = MTLG.getStageContainer();

    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill("#FFF").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);

    var requestButton;
    // Text
    // the text for the title depends on the role.
    var titleText;
    if(master) titleText = locText("master");
    else titleText = locText("slave");

    var title = new createjs.Text(titleText, "40px Arial", "Black");
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 0.5 * MTLG.getOptions().width;
    title.y = 0.1 * MTLG.getOptions().height;

    if(master){
      // If master the blueCircle will be created
      blueCircle = MTLG.utils.gfx.getShape();
      blueCircle.graphics.beginFill('blue').drawCircle(0,0,50);
      blueCircle.x = 0.5 * MTLG.getOptions().width;
      blueCircle.y = 0.5 * MTLG.getOptions().height;
      // Add listener to be able to move the blueCircle
      blueCircle.on('pressmove', pressMoveFunction);

      // The blueCircle should be shared, therefore in the next line an
      // SharedObject from the blueCircle will be instantiated
      blueCircleObject = new MTLG.distributedDisplays.SharedObject(blueCircle,0);
      // Now a custom function will be registered. If another client sends a
      // customAction with the parameter "requestBlueCircle", the function
      // customActionRequestBlueCircle will be called.
      MTLG.distributedDisplays.actions.setCustomFunction("requestBlueCircle", customActionRequestBlueCircle);
    }else{
      // a function ("customActionReceiveBlueCircle") for the answer will be registered
      MTLG.distributedDisplays.actions.registerListenerForAction('addSharedObjects', addSharedObjectsListener);

      // if not master, the blueCircle will be requested from the master.
      // for the request we need the clientId of this client
      var id = MTLG.distributedDisplays.rooms.getClientId();

      requestButton = MTLG.utils.uiElements.addButton({
        text: locText("request"),
        sizeX: MTLG.getOptions().width * 0.1,
        sizeY: 70,
        textColor: 'black',
        bgColor1: 'white',
        bgColor2: 'red'
      }, function() {
        // send request: first parameter is the reciever, to whom the message
        // should go. In this case it is the whole room, since we do not know the
        // clientID of the master. But still only the master will answer since
        // only the master registers a listener for this customAction.
        // The second parameter is the message identifier
        // The third parameter is the clientId . We send this so that the master
        // can send an answer to only us and not the whole room.
        MTLG.distributedDisplays.communication.sendCustomAction("room","requestBlueCircle",id);
        stage.removeChild(requestButton);
      });
      requestButton.x = requestButton.getBounds().width / 2 + 10;
      requestButton.y = requestButton.getBounds().height / 2 + 10;
    }

    var backButton = MTLG.utils.uiElements.addButton({
      text: locText("back"),
      sizeX: MTLG.getOptions().width * 0.1,
      sizeY: 70,
      textColor: 'black',
      bgColor1: 'white',
      bgColor2: 'red'
    }, function(){
      // Leave the room and delete the sharedObject, so that no updates will be
      // received in the future.
      MTLG.distributedDisplays.rooms.leaveRoom('room', function (result) {
          if (result && result.success) {
              MTLG.distributedDisplays.actions.deregisterListenerForAction('addSharedObjects', addSharedObjectsListener);
              MTLG.distributedDisplays.actions.removeCustomFunction("requestBlueCircle", customActionRequestBlueCircle);
              MTLG.utils.inactivity.removeAllListeners();
              MTLG.lc.goToMenu();
          } else {
              swal({
                  type: 'error',
                  title: locText("field1_leaveRoomErrorTitle"),
                  text: result.reason,
                  focusConfirm: true
              });
          }
      });

    });
    backButton.x =  MTLG.getOptions().width - MTLG.getOptions().width * 0.1;
    backButton.y = MTLG.getOptions().height - 70;

    // Add objects to stage
    stage.addChild(background);
    stage.addChild(title);
    if(master) stage.addChild(blueCircle);
    else stage.addChild(requestButton);
    //Back button to go to menu
    stage.addChild(backButton);
};


var addSharedObjectsListener = function ({action, added, from, toAdd}) {
  var sharedId = Object.keys(added)[0];

  //Get SharedObject in order to add it to the stage container and save it for later.
  blueCircleObject = blueCircleObject || MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);

  // if the slave should also be able to move the object, the listener must be
  // registered (again), since the functionality cannot be sent.
  blueCircleObject.createJsObject.on('pressmove', pressMoveFunction);

  // Set the "requestBlueCircle" listener, so that even when the master leaves
  // the room, this client can send the blueCircle to new clients.
  MTLG.distributedDisplays.actions.setCustomFunction("requestBlueCircle", customActionRequestBlueCircle);
};

/**
 * This function is called by the master, when he gets a request.
 */
var customActionRequestBlueCircle = function(requester) {
  MTLG.distributedDisplays.communication.sendSharedObject(requester, blueCircleObject, false, false);
}

/**
 * This function is a listener for the blue circle.
 * It updates the location of the Object of the listener.
 */
var pressMoveFunction = function({localX, localY}){
    this.set(this.localToLocal(localX, localY, this.parent));
};


module.exports = {
  checkLevel1: checkLevel1,
  level1_init: level1_init
}
