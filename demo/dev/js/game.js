/**
 * @Date:   2019-03-19T10:14:00+01:00
 * @Last modified time: 2019-05-29T15:44:28+02:00
 */

// expose framework for debugging
window.MTLG = MTLG;
// ignite the framework
document.addEventListener("DOMContentLoaded", MTLG.init);

// external dependencies
var swal = require('sweetalert2');

// add moduls
require('mtlg-modul-menu');
require('mtlg-moduls-utilities');
require('mtlg-module-dd');

// load configuration
require('../manifest/device.config.js');
require('../manifest/game.config.js');
require('../manifest/game.settings.js');

// load translations
require('../lang/lang.js');

const menu = require('./menu/menu.js');
const basic = require('./level/basic/level1.js');
const masterSlaves = {
  lobby : require('./level/masterSlaves/lobby.js'),
  gamefieldMaster : require('./level/masterSlaves/gamefieldMaster.js'),
  gamefieldSlave : require('./level/masterSlaves/gamefieldSlave.js'),
  roleallocation : require('./level/masterSlaves/roleallocation.js')
};
const linkedAreaDemo = require('./level/linkedAreaDemo/level1.js');
const dotGame = {
  level1 : require('./level/dotGame/render1.js'),
  level2 : require('./level/dotGame/render2.js'),
  level3 : require('./level/dotGame/render3.js')
};
const qrcode = require('./level/qrcode/qrcodelevel1.js');
/**
 * Init function for the game.
 * @params options: The options parsed from the manifest files/ Defaults.
 */
var initGame = function(pOptions){
  //Initialize game

  // Register main menu
  MTLG.lc.registerMenu(menu.drawMainMenu);

  // Register levels
  // basic
  MTLG.lc.registerLevel(basic.level1_init, basic.checkLevel1);

  // masterSlaves
  MTLG.lc.registerLevel(masterSlaves.lobby.init, masterSlaves.lobby.check);
  MTLG.lc.registerLevel(masterSlaves.roleallocation.init, masterSlaves.roleallocation.check);
  MTLG.lc.registerLevel(masterSlaves.gamefieldMaster.init, masterSlaves.gamefieldMaster.check);
  MTLG.lc.registerLevel(masterSlaves.gamefieldSlave.init, masterSlaves.gamefieldSlave.check);

  // linkedAreaDemo
  MTLG.lc.registerLevel(linkedAreaDemo.level1_init, linkedAreaDemo.checkLevel1);

  // dotGame
  MTLG.lc.registerLevel(dotGame.level1.init, dotGame.level1.check);
  MTLG.lc.registerLevel(dotGame.level2.init, dotGame.level2.check);
  MTLG.lc.registerLevel(dotGame.level3.init, dotGame.level3.check);

  // qrcode
  MTLG.lc.registerLevel(qrcode.level1_init, qrcode.checkLevel1);

  //Init is done
  console.log("Game Init function called");
}

//Register Init function with the MTLG framework
//The function passed to addGameInit will be used to initialize the game.
//Use the initialization to register levels and menus.
MTLG.addGameInit(initGame);
