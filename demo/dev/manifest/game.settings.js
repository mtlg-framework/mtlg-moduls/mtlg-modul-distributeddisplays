/**
 * @Date:   2019-03-07T14:05:39+01:00
 * @Last modified time: 2019-05-15T13:51:27+02:00
 */



MTLG.loadSettings({
  default: {
    cards_height: 180,
    cards_width: 120
  },
  all: {
    // add all settings to get it in a settings menu
  }
});
