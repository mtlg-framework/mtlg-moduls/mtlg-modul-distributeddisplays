
/**
 * Returns the default configuration for syncProperties
 * @returns {object} The default configuration for syncProperties
 * @private
 * @alias SharedObject.scheme
 */
// Complex types are defined by className, primitive types are stored as is and are either commented with their type or a Number if theres no comment
// ClassNames with a capital letter are real classes, classNames with a small letter are just ordinary objects / values tagged for special handling
let getPropertiesTemplate = function() {
    return {
        // DisplayObject
        _bounds: {
            send: true, receive: true, className: "Rectangle",
            subProperties: {
                x:      { send: true, receive: true },
                y:      { send: true, receive: true },
                width:  { send: true, receive: true },
                height: { send: true, receive: true }
            }
        },
        alpha:              { send: true, receive: true },
        cacheID:            { send: true, receive: true, className: "cache" },
        compositeOperation: { send: true, receive: true }, // Type string
        cursor:             { send: true, receive: true }, // Type string
        filters: {
            send: true, receive: true, className: "Array", // Array of Filters
            itemClasses: {
                AlphaMapFilter: {
                    send: true, receive: true, className: "AlphaMapFilter",
                    subProperties: {
                        alphaMap: { send: true, receive: true, className: "image" }
                    }
                },
                AlphaMaskFilter: {
                    send: true, receive: true, className: "AlphaMaskFilter",
                    subProperties: {
                        mask: { send: true, receive: true, className: "image" }
                    }
                },
                BlurFilter: {
                    send: true, receive: true, className: "BlurFilter",
                    subProperties: {
                        blurX:   { send: true, receive: true },
                        blurY:   { send: true, receive: true },
                        quality: { send: true, receive: true }
                    }
                },
                ColorFilter: {
                    send: true, receive: true, className: "ColorFilter",
                    subProperties: {
                        alphaMultiplier: { send: true, receive: true },
                        alphaOffset:     { send: true, receive: true },
                        blueMultiplier:  { send: true, receive: true },
                        blueOffset:      { send: true, receive: true },
                        greenMultiplier: { send: true, receive: true },
                        greenOffset:     { send: true, receive: true },
                        redMultiplier:   { send: true, receive: true },
                        redOffset:       { send: true, receive: true }
                    }
                },
                ColorMatrixFilter: {
                    send: true, receive: true, className: "ColorMatrixFilter",
                    subProperties: {
                        matrix: {
                            send: true, receive: true, className: "ColorMatrix",
                            subProperties: {
                                "0":  { send: true, receive: true },
                                "1":  { send: true, receive: true },
                                "2":  { send: true, receive: true },
                                "3":  { send: true, receive: true },
                                "4":  { send: true, receive: true },
                                "5":  { send: true, receive: true },
                                "6":  { send: true, receive: true },
                                "7":  { send: true, receive: true },
                                "8":  { send: true, receive: true },
                                "9":  { send: true, receive: true },
                                "10": { send: true, receive: true },
                                "11": { send: true, receive: true },
                                "12": { send: true, receive: true },
                                "13": { send: true, receive: true },
                                "14": { send: true, receive: true },
                                "15": { send: true, receive: true },
                                "16": { send: true, receive: true },
                                "17": { send: true, receive: true },
                                "18": { send: true, receive: true },
                                "19": { send: true, receive: true },
                                "20": { send: true, receive: true },
                                "21": { send: true, receive: true },
                                "22": { send: true, receive: true },
                                "23": { send: true, receive: true },
                                "24": { send: true, receive: true }
                            }
                        }
                    }
                },
                length: { send: true, receive: true }, // Technically required
                other: { send: false, receive: false } // No other types are synced
            }
        },
        hitArea:      { send: true, receive: true, className: "SharedObject" },
        mask:         { send: true, receive: true, className: "SharedObject" },
        mouseEnabled: { send: true, receive: true }, // Type boolean
        name:         { send: true, receive: true }, // Type string
        regX:         { send: true, receive: true },
        regY:         { send: true, receive: true },
        rotation:     { send: true, receive: true },
        scaleX:       { send: true, receive: true },
        scaleY:       { send: true, receive: true },
        shadow:       { send: true, receive: true, className: "Shadow",
            subProperties: {
                blur:    { send: true, receive: true },
                color:   { send: true, receive: true }, // Type string
                offsetX: { send: true, receive: true },
                offsetY: { send: true, receive: true }
            }
        },
        sharedId:    { send: true, receive: true }, // Type string
        skewX:       { send: true, receive: true },
        skewY:       { send: true, receive: true },
        snapToPixel: { send: true, receive: true }, // Type boolean
        tickEnabled: { send: true, receive: true }, // Type boolean
        transformMatrix: {
            send: true, receive: true, className: "Matrix2D",
            subProperties: {
                a:  { send: true, receive: true },
                b:  { send: true, receive: true },
                c:  { send: true, receive: true },
                d:  { send: true, receive: true },
                tx: { send: true, receive: true },
                ty: { send: true, receive: true },
            }
        },
        visible: { send: true, receive: true }, // Type boolean
        x:       { send: true, receive: true },
        y:       { send: true, receive: true },

        // Bitmap
        image: { send: true, receive: true, className: "image" },
        sourceRect: {
            send: true, receive: true, className: "Rectangle",
            subProperties: {
                x:      { send: true, receive: true },
                y:      { send: true, receive: true },
                width:  { send: true, receive: true },
                height: { send: true, receive: true }
            }
        },

        // Container
        children: {
            send: true, receive: true, className: "Array", // Array of SharedObjects
            itemClasses: {
                SharedObject: { send: true, receive: true, className: "SharedObject" },
                length: { send: true, receive: true } // Technically required
                // No other types are synced
                // other: {send: false, receive: false}
            }
        },
        mouseChildren: { send: true, receive: true }, // Type boolean
        tickChildren:  { send: true, receive: true }, // Type boolean

        // Shape
        graphics: {
            send: false, receive: false, className: "Graphics",
            subProperties: {
                _instructions: {
                    send: true, receive: true, className: "Array", // Array of Commands
                    itemClasses: {
                        Arc: {
                            send: true, receive: true, className: "Arc",
                            subProperties: {
                                anticlockwise: { send: true, receive: true }, // Type boolean
                                endAngle:      { send: true, receive: true },
                                radius:        { send: true, receive: true },
                                startAngle:    { send: true, receive: true },
                                x:             { send: true, receive: true },
                                y:             { send: true, receive: true }
                            }
                        },
                        ArcTo: {
                            send: true, receive: true, className: "ArcTo",
                            subProperties: {
                                radius: { send: true, receive: true },
                                x1:     { send: true, receive: true },
                                x2:     { send: true, receive: true },
                                y1:     { send: true, receive: true },
                                y2:     { send: true, receive: true }
                            }
                        },
                        BeginPath: {
                            send: true, receive: true, className: "BeginPath"
                        },
                        BezierCurveTo: {
                            send: true, receive: true, className: "BezierCurveTo",
                            subProperties: {
                                cp1x: { send: true, receive: true },
                                cp1y: { send: true, receive: true },
                                cp2x: { send: true, receive: true },
                                cp2y: { send: true, receive: true },
                                x:    { send: true, receive: true },
                                y:    { send: true, receive: true }
                            }
                        },
                        Circle: {
                            send: true, receive: true, className: "Circle",
                            subProperties: {
                                radius: { send: true, receive: true },
                                x:      { send: true, receive: true },
                                y:      { send: true, receive: true }
                            }
                        },
                        ClosePath: {
                            send: true, receive: true, className: "ClosePath",
                        },
                        Ellipse: {
                            send: true, receive: true, className: "Ellipse",
                            subProperties: {
                                h: { send: true, receive: true },
                                w: { send: true, receive: true },
                                x: { send: true, receive: true },
                                y: { send: true, receive: true }
                            }
                        },
                        Fill: {
                            send: true, receive: true, className: "Fill",
                            subProperties: {
                                matrix: {
                                    send: true, receive: true, className: "Matrix2D",
                                    subProperties: {
                                        a:  { send: true, receive: true },
                                        b:  { send: true, receive: true },
                                        c:  { send: true, receive: true },
                                        d:  { send: true, receive: true },
                                        tx: { send: true, receive: true },
                                        ty: { send: true, receive: true }
                                    }
                                },
                                style: { // Type object, set by the various kinds of fill
                                    send: true, receive: true, className: "style",
                                    subProperties: {
                                        props: {
                                            send: true, receive: true, className: "styleProps",
                                            subProperties: {
                                                // linearGradient
                                                colors: {
                                                    send: true, receive: true, className: "Array", // Array of color strings
                                                    itemClasses: {
                                                        length: { send: true, receive: true },
                                                        other:  { send: true, receive: true }
                                                    }
                                                },
                                                ratios: {
                                                    send: true, receive: true, className: "Array", // Array of numbers
                                                    itemClasses: {
                                                        length: { send: true, receive: true },
                                                        other:  { send: true, receive: true }
                                                    }
                                                },
                                                type: { send: true, receive: true }, // Type string
                                                x0:   { send: true, receive: true },
                                                x1:   { send: true, receive: true },
                                                y0:   { send: true, receive: true },
                                                y1:   { send: true, receive: true },
                                                // radialGradient (additional)
                                                r0:   { send: true, receive: true },
                                                r1:   { send: true, receive: true },
                                                // bitmap
                                                image:      { send: true, receive: true, className: "image" },
                                                repetition: { send: true, receive: true } // Type string
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        LineTo: {
                            send: true, receive: true, className: "LineTo",
                            subProperties: {
                                x: { send: true, receive: true },
                                y: { send: true, receive: true }
                            }
                        },
                        MoveTo: {
                            send: true, receive: true, className: "MoveTo",
                            subProperties: {
                                x: { send: true, receive: true },
                                y: { send: true, receive: true }
                            }
                        },
                        PolyStar: {
                            send: true, receive: true, className: "PolyStar",
                            subProperties: {
                                angle:     { send: true, receive: true },
                                pointSize: { send: true, receive: true },
                                radius:    { send: true, receive: true },
                                sides:     { send: true, receive: true },
                                x:         { send: true, receive: true },
                                y:         { send: true, receive: true }
                            }
                        },
                        QuadraticCurveTo: {
                            send: true, receive: true, className: "QuadraticCurveTo",
                            subProperties: {
                                cpx: { send: true, receive: true },
                                cpy: { send: true, receive: true },
                                x:   { send: true, receive: true },
                                y:   { send: true, receive: true }
                            }
                        },
                        Rect: {
                            send: true, receive: true, className: "Rect",
                            subProperties: {
                                h: { send: true, receive: true },
                                w: { send: true, receive: true },
                                x: { send: true, receive: true },
                                y: { send: true, receive: true }
                            }
                        },
                        RoundRect: {
                            send: true, receive: true, className: "RoundRect",
                            subProperties: {
                                h:        { send: true, receive: true },
                                radiusBL: { send: true, receive: true },
                                radiusBR: { send: true, receive: true },
                                radiusTL: { send: true, receive: true },
                                radiusTR: { send: true, receive: true },
                                w:        { send: true, receive: true },
                                x:        { send: true, receive: true },
                                y:        { send: true, receive: true }
                            }
                        },
                        Stroke: {
                            send: true, receive: true, className: "Stroke",
                            subProperties: {
                                ignoreScale: { send: true, receive: true }, // Type boolean
                                style: {
                                    send: true, receive: true, className: "style",
                                    subProperties: {
                                        props: {
                                            send: true, receive: true, className: "styleProps",
                                            subProperties: {
                                                // linearGradient
                                                colors: {
                                                    send: true, receive: true, className: "Array", // Array of color strings
                                                    itemClasses: {
                                                        length: { send: true, receive: true },
                                                        other:  { send: true, receive: true }
                                                    }
                                                },
                                                ratios: {
                                                    send: true, receive: true, className: "Array", // Array of numbers
                                                    itemClasses: {
                                                        length: { send: true, receive: true },
                                                        other:  { send: true, receive: true }
                                                    }
                                                },
                                                type: { send: true, receive: true }, // Type string
                                                x0:   { send: true, receive: true },
                                                x1:   { send: true, receive: true },
                                                y0:   { send: true, receive: true },
                                                y1:   { send: true, receive: true },
                                                // radialGradient (additional)
                                                r0:   { send: true, receive: true },
                                                r1:   { send: true, receive: true },
                                                // bitmap
                                                image:      { send: true, receive: true, className: "image" },
                                                repetition: { send: true, receive: true } // Type string
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        StrokeDash: {
                            send: true, receive: true, className: "StrokeDash",
                            subProperties: {
                                offset: { send: true, receive: true },
                                segments: {
                                    send: true, receive: true, className: "Array", // Array of numbers
                                    itemClasses: {
                                        length: { send: true, receive: true },
                                        other:  { send: true, receive: true }
                                    }
                                }
                            }
                        },
                        StrokeStyle: {
                            send: true, receive: true, className: "StrokeStyle",
                            subProperties: {
                                caps:        { send: true, receive: true }, // Type string
                                ignoreScale: { send: true, receive: true }, // Type boolean
                                joints:      { send: true, receive: true }, // Type string
                                miterLimit:  { send: true, receive: true },
                                width:       { send: true, receive: true }
                            }
                        },
                        length: { send: true, receive: true }, // Technically required
                        other:  { send: true, receive: true } // Custom Command objects
                    }
                }
            }
        },

        // Text
        color:        { send: true, receive: true }, // Type string
        font:         { send: true, receive: true }, // Type string
        lineHeight:   { send: true, receive: true },
        lineWidth:    { send: true, receive: true },
        maxWidth:     { send: true, receive: true },
        outline:      { send: true, receive: true },
        text:         { send: true, receive: true }, // Type string
        textAlign:    { send: true, receive: true }, // Type string
        textBaseline: { send: true, receive: true }, // Type string
    };
};
/**
 * Reference to lookup default entries of syncProperties. Stored to save recreation of the whole object.
 * @readonly
 * @private
 * @type {object}
 * @alias SharedObject.defaultSyncProperties
 */
var defaultSyncProperties = getPropertiesTemplate();



/**
 * Converts the given sendable value to a real value
 * @param {any} value The sendable value to convert
 * @param {{className: string, subProperties: object}} options Options influencing the way the value is converted. Considered are .className and .subProperties, but they are both optional.
 * @param {SharedObject} sharedObject The SharedObject to contain the value. Sometimes needed to set additional information (e.g. for cache).
 * @returns {any} The converted value
 * @private
 * @alias SharedObject.deserialize
 */
let deserialize = function (value, options, sharedObject) {
   let res = null; // Create base value
   var c = createjs, g = c.Graphics; // Shortcuts
   var done = {}; // Contains subProperties, that are already converted
   if (options.className && options.className != "") {
       switch (options.className) {
           case "AlphaMapFilter":
               if (value) {
                   var img = deserialize(value.alphaMap, { className: "image" }, sharedObject);
                   res = new c.AlphaMapFilter(img);
                   done.alphaMap = true;
               }
               break;
           case "AlphaMaskFilter":
               if (value) {
                   var img = deserialize(value.mask, { className: "image" }, sharedObject);
                   res = new c.AlphaMaskFilter(img);
                   done.mask = true;
               }
               break;
           case "Array":
               if (value) {
                   res = [];
                   // Convert each array entry from its sendable
                   for (var p in value) {
                       if (value.hasOwnProperty(p)) {
                           if (value[p]) {
                               if (options.itemClasses && options.itemClasses.hasOwnProperty("SharedObject")) { // Special handling for Arrays of SharedObjects
                                   res[p] = value[p];
                               } else {
                                   var opts = (options.itemClasses && options.itemClasses[value[p].className]) ? options.itemClasses[value[p].className] : { className: value[p].className };
                                   res[p] = deserialize(value[p].value, opts, sharedObject);
                               }
                           } else res[p] = value[p];
                       }
                   }
               }
               break;
           case "BlurFilter":
               if (value) res = new c.BlurFilter(value.blurX, value.blurY, value.quality);
               break;
           case "ColorFilter":
               if (value) res = new c.ColorFilter(value.redMultiplier, value.greenMultiplier, value.blueMultiplier, value.alphaMultiplier, value.redOffset, value.greenOffset, value.blueOffset, value.alphaOffset);
               break;
           case "ColorMatrix":
               if (value) {
                   res = new c.ColorMatrix(0, 0, 0, 0); // Create a new ColorMatrix, setting adjustments to 0 skips some calculations
                   res.copy(value); // Set the received array of values
               }
               break;
           case "ColorMatrixFilter":
               if (value) {
                   // Create a new ColorMatrix
                   let m = deserialize(value.matrix, { className: "ColorMatrix" }, sharedObject);
                   // Create the ColorMatrixFilter
                   res = new c.ColorMatrixFilter(m);
                   done.matrix = true;
               }
               break;
           case "cache":
               // Prevent triggering an update for the cacheID
               sharedObject.justReceived = {
                   propertyPath: "cacheID",
                   newValue: value ? createjs.DisplayObject._nextCacheID : 0
               };
               if (value) {
                   sharedObject.createJsObject._filterOffsetX = value._filterOffsetX;
                   sharedObject.createJsObject._filterOffsetY = value._filterOffsetY;
                   sharedObject.createJsObject.cache(value._cacheOffsetX, value._cacheOffsetY, value._cacheWidth, value._cacheHeight, value._cacheScale);
                   // Even if the "_"-values have not changed and only .updateCache() would be needed, this ensures all values are correct and then calls updateCache()
                   // It also ensures a cacheCanvas is existing, but does not create a new one if already existing
               } else {
                   // No more caching
                   sharedObject.createJsObject.uncache();
               }
               res = sharedObject.createJsObject.cacheID;
               break;
           case "Graphics":
               // A reference to the existing graphics property is needed here to insert the commands into the right array
               if (value) res = sharedObject.createJsObject.graphics;
               break;
           case "image":
               if (value) {
                   if(value.assetId) {
                       res = MTLG.assets.getAsset(value.assetId);
                   } else {
                       res = document.createElement(value.tagName);
                       res.src = value.src;
                   }
               }
               break;
           case "Matrix2D":
               if (value) {
                   res = new c.Matrix2D();
                   res.copy(value); // Copies a, b, c, d, tx and ty
               }
               break;
           case "Rectangle":
               if (value) {
                   res = new c.Rectangle();
                   res.copy(value); // Copies x, y, width and height
               }
               break;
           case "Shadow":
               if (value) res = new c.Shadow(value.color, value.offsetX, value.offsetY, value.blur);
               break;
           case "SharedObject":
               if (value) res = value; // Value contains only the sharedId
               break;


           // Graphic Commands
           case "Arc":              if (value) res = new g.Arc(value.x, value.y, value.radius, value.startAngle, value.endAngle, value.anticlockwise);
               break;
           case "ArcTo":            if (value) res = new g.ArcTo(value.x1, value.y1, value.x2, value.y2, value.radius);
               break;
           case "BeginPath":        if (value) res = g.beginCmd; // Reusing the static command object to avoid unnecessary instantiation
               break;
           case "BezierCurveTo":    if (value) res = new g.BezierCurveTo(value.cp1x, value.cp1y, value.cp2x, value.cp2y, value.x, value.y);
               break;
           case "Circle":           if (value) res = new g.Circle(value.x, value.y, value.radius);
               break;
           case "ClosePath":        if (value) res = new g.ClosePath();
               break;
           case "Ellipse":          if (value) res = new g.Ellipse(value.x, value.y, value.w, value.h);
               break;
           case "LineTo":           if (value) res = new g.LineTo(value.x, value.y);
               break;
           case "MoveTo":           if (value) res = new g.MoveTo(value.x, value.y);
               break;
           case "PolyStar":         if (value) res = new g.PolyStar(value.x, value.y, value.radius, value.sides, value.pointSize, value.angle);
               break;
           case "QuadraticCurveTo": if (value) res = new g.QuadraticCurveTo(value.cpx, value.cpy, value.x, value.y);
               break;
           case "Rect":             if (value) res = new g.Rect(value.x, value.y, value.w, value.h);
               break;
           case "RoundRect":        if (value) res = new g.RoundRect(value.x, value.y, value.w, value.h, value.radiusTL, value.radiusTR, value.radiusBR, value.radiusBL);
               break;
           case "StrokeStyle":      if (value) res = new g.StrokeStyle(value.width, value.caps, value.joints, value.miterLimit, value.ignoreScale);
               break;
           case "Fill":
           case "Stroke":
               if (value) {
                   // Create base object, style is set for both in the same way in the next step
                   if (options.className === "Fill") res = new g.Fill(undefined, deserialize(value.matrix, { className: "Matrix2D" }, sharedObject)); // New Fill with undefined style and converted matrix
                   else res = new g.Stroke(undefined, value.ignoreScale); // else options.className === "Stroke"
                   // Set style
                   if (value.style === null || value.style === undefined || typeof value.style === "string") res.style = value.style;
                   else {
                       var col, rat, p = value.style.props;
                       if (p.colors) col = deserialize(p.colors, { className: "Array" }, sharedObject);
                       if (p.ratios) rat = deserialize(p.ratios, { className: "Array" }, sharedObject);
                       switch (p.type) {
                           case "linear": res.linearGradient(col, rat, p.x0, p.y0, p.x1, p.y1);
                               break;
                           case "radial": res.radialGradient(col, rat, p.x0, p.y0, p.r0, p.x1, p.y1, p.r1);
                               break;
                           case "bitmap":
                               var img = deserialize(p.image, { className: "image" }, sharedObject);
                               res.bitmap(img, p.repetition);
                               break;
                           default: MTLG.warn("ddd", "SharedObject.deserialize: No behavior defined for the", p.type, "type of Fill or Stroke.", value);
                               break;
                       }
                   }
                   done.matrix = true;
                   done.style = true;
               }
               break;
           case "StrokeDash":
               if (value) {
                   var opts = {
                       className: "Array",
                       itemClasses: {
                           length: { send: true, receive: true },
                           other: { send: true, receive: true }
                       }
                   };
                   res = new g.StrokeDash(deserialize(value.segments, opts, sharedObject), value.offset);
                   done.segments = true;
               }
               break;
           case "style":
               if (value) {
                   if (typeof value === "string") res = value; // Fill or Stroke style defined as color string
                   else res = {}; // gradient or bitmap type defined by props
               }
               break;
           case "styleProps":
               if (value) {
                   if (options.sharedGrandParent) {
                       var col,
                           rat,
                           p = value, // shorthand for the props object
                           fillStroke = options.sharedGrandParent; // sharedParent contains a reference to the grandparent Fill or Stroke object
                       if (p.colors) col = deserialize(p.colors, { className: "Array" }, sharedObject);
                       if (p.ratios) rat = deserialize(p.ratios, { className: "Array" }, sharedObject);
                       sharedObject.justReceived.ignoreAll = true; // Workaround that is sadly required since CreateJS changes multiple properties during the change of Fill or Stroke
                       switch (p.type) {
                           case "linear": fillStroke.linearGradient(col, rat, p.x0, p.y0, p.x1, p.y1);
                               break;
                           case "radial": fillStroke.radialGradient(col, rat, p.x0, p.y0, p.r0, p.x1, p.y1, p.r1);
                               break;
                           case "bitmap":
                               var img = deserialize(p.image, { className: "image" }, sharedObject);
                               fillStroke.bitmap(img, p.repetition);
                               break;
                           default: MTLG.warn("ddd", "SharedObject.deserialize: No behavior defined for the", p.type, "type of Fill or Stroke.", value);
                               break;
                       }
                       sharedObject.justReceived.ignoreAll = false;
                       res = fillStroke.style.props;
                       done.colors = true;
                       done.ratios = true;
                       done.image = true;
                   } else res = {};
               }
               break;

           // Primitive types
           case "Boolean":
           case "Number":
           case "String":
               res = value;
               break;
           case "Object":
               res = {}; // Real value stored in subProperties
               break;
           default:
               MTLG.warn("ddd", "SharedObject.deserialize: No converting defined for className ", options.className);
               res = value;
               break;
       }
   } else {
       // Copy value if theres no class to initialize, e.g. for primitive values
       res = value;
   }

   // Attach subproperties
   if (options.subProperties) {
       for (let p in options.subProperties) {
           if (value !== null && value !== undefined && value.hasOwnProperty(p) && !done[p]) {
               res[p] = deserialize(value[p], options.subProperties[p], sharedObject);
           }
       }
   }
   return res;
};



/**
 * Gets the name of the class the given object is an instance of
 * Reverse of {@link SharedObject.instantiateClass}.
 * @param {any} obj The object with the class name to get
 * @returns {string} The class name of the given object
 * @private
 * @alias SharedObject.getClassName
 */
let getClassName = function (obj) {
    // Return stored value if already computed
    if (obj === null || obj === undefined) return "";
    // Compare obj with known classes
    var className = "", c = createjs, g = c.Graphics;
    switch (obj.constructor) {
        case Array:               className = "Array"; break;
        case Boolean:             className = "Boolean"; break;
        case Number:              className = "Number"; break;
        case Object:              className = "Object"; break;
        case String:              className = "String"; break;
        case c.AlphaMapFilter:    className = "AlphaMapFilter"; break;
        case c.AlphaMaskFilter:   className = "AlphaMaskFilter"; break;
        case c.Bitmap:            className = "Bitmap"; break;
        case c.BlurFilter:        className = "BlurFilter"; break;
        case c.ColorFilter:       className = "ColorFilter"; break;
        case c.ColorMatrix:       className = "ColorMatrix"; break;
        case c.ColorMatrixFilter: className = "ColorMatrixFilter"; break;
        case c.Container:         className = "Container"; break;
        case c.DisplayObject:     className = "DisplayObject"; break;
        case c.Matrix2D:          className = "Matrix2D"; break;
        case c.Rectangle:         className = "Rectangle"; break;
        case c.Shadow:            className = "Shadow"; break;
        case c.Shape:             className = "Shape"; break;
        case c.Text:              className = "Text"; break;
        case g.Arc:               className = "Arc"; break;
        case g.ArcTo:             className = "ArcTo"; break;
        case g.BeginPath:         className = "BeginPath"; break;
        case g.BezierCurveTo:     className = "BezierCurveTo"; break;
        case g.Circle:            className = "Circle"; break;
        case g.ClosePath:         className = "ClosePath"; break;
        case g.Ellipse:           className = "Ellipse"; break;
        case g.Fill:              className = "Fill"; break;
        case g.LineTo:            className = "LineTo"; break;
        case g.MoveTo:            className = "MoveTo"; break;
        case g.PolyStar:          className = "PolyStar"; break;
        case g.QuadraticCurveTo:  className = "QuadraticCurveTo"; break;
        case g.Rect:              className = "Rect"; break;
        case g.RoundRect:         className = "RoundRect"; break;
        case g.Stroke:            className = "Stroke"; break;
        case g.StrokeDash:        className = "StrokeDash"; break;
        case g.StrokeStyle:       className = "StrokeStyle"; break;
        default: MTLG.warn("ddd", "SharedObject.getClassName: Unknown class.", obj); break;
    }
    return className;
};




/**
 * Creates a new instance of the class with the given name.
 * Reverse of {@link SharedObject.getClassName}.
 * @param {string} name The class name of the object to create
 * @returns {any} The created object of the referenced class
 * @private
 * @alias SharedObject.instantiateClass
 */
var instantiateClass = function (name) {
    var res;
    var c = createjs, g = c.Graphics; // Shortcuts
    switch (name) {
        case "Array":               res = []; break; // Set some default values
        case "Boolean":             res = true; break; // for the primitive types
        case "Number":              res = 42; break; // instead of
        case "Object":              res = {}; break; // creating
        case "String":              res = ""; break; // object wrappers
        case "AlphaMapFilter":      res = new c.AlphaMapFilter(); break;
        case "AlphaMaskFilter":     res = new c.AlphaMaskFilter(); break;
        case "Bitmap":              res = new c.Bitmap(); break;
        case "BlurFilter":          res = new c.BlurFilter(); break;
        case "ColorFilter":         res = new c.ColorFilter(); break;
        case "ColorMatrix":         res = new c.ColorMatrix(); break;
        case "ColorMatrixFilter":   res = new c.ColorMatrixFilter(); break;
        case "Container":           res = new c.Container(); break;
        case "DisplayObject":       res = new c.DisplayObject(); break;
        case "Matrix2D":            res = new c.Matrix2D(); break;
        case "Rectangle":           res = new c.Rectangle(); break;
        case "Shadow":              res = new c.Shadow(); break;
        case "Shape":               res = new c.Shape(); break;
        case "Text":                res = new c.Text(); break;
        case "Arc":                 res = new g.Arc(); break;
        case "ArcTo":               res = new g.ArcTo(); break;
        case "BeginPath":           res = new g.BeginPath(); break;
        case "BezierCurveTo":       res = new g.BezierCurveTo(); break;
        case "Circle":              res = new g.Circle(); break;
        case "ClosePath":           res = new g.ClosePath(); break;
        case "Ellipse":             res = new g.Ellipse(); break;
        case "Fill":                res = new g.Fill(); break;
        case "LineTo":              res = new g.LineTo(); break;
        case "MoveTo":              res = new g.MoveTo(); break;
        case "PolyStar":            res = new g.PolyStar(); break;
        case "QuadraticCurveTo":    res = new g.QuadraticCurveTo(); break;
        case "Rect":                res = new g.Rect(); break;
        case "RoundRect":           res = new g.RoundRect(); break;
        case "Stroke":              res = new g.Stroke(); break;
        case "StrokeDash":          res = new g.StrokeDash(); break;
        case "StrokeStyle":         res = new g.StrokeStyle(); break;
        default: MTLG.warn("ddd", "SharedObject.instantiateClass: Unknown class name.", name); return null;
    }

    return res;
};



/**
 * Converts the given value to a sendable value.
 * @param {any} value The value to convert
 * @param {{className: string, subProperties: object}} options Options influencing the way the value is converted. Considered are .className and .subProperties, but they are both optional
 * @param {SharedObject} sharedObject The SharedObject containing the value. Sometimes needed to lookup additional information (e.g. for cache).
 * @returns {any} The converted and sendable value
 * @private
 * @alias SharedObject.serialize
 */
var serialize = function (value, options, sharedObject) {
    // Create base value
    let res = value ? {} : value; // Prepare basic object if value has something to store, otherwise use value
    var filters = defaultSyncProperties.filters.itemClasses,
        graphics = defaultSyncProperties.graphics.subProperties._instructions.itemClasses; // Shortcuts
    // subProperties may be added for some classes that require recursively applying the scheme
    let subProperties = options.subProperties;
    if (options.className && options.className != "") {
        switch (options.className) {
            // More complex types
            case "Array":
                if (value) {
                    res = [];
                    // Convert each array entry to its sendable
                    for (var p in value) {
                        if (value.hasOwnProperty(p)) {
                            if ((value[p] || value[p] === 0) && p !== "length") {
                                if (options.itemClasses && options.itemClasses.hasOwnProperty("SharedObject")) { // Special handling for Arrays of SharedObjects
                                    res[p] = value[p].sharedId;
                                } else {
                                    res[p] = {
                                        className: getClassName(value[p]), // Storing the class name is needed for array items as that is not stored in .syncProperties
                                        value: serialize(value[p], { className: getClassName(value[p]) }, sharedObject)
                                    };
                                }
                            } else res[p] = value[p];
                        }
                    }
                }
                break;
            case "cache":
                if (sharedObject.createJsObject.cacheCanvas) {
                    res = {
                        _cacheHeight:   sharedObject.createJsObject._cacheHeight,
                        _cacheOffsetX:  sharedObject.createJsObject._cacheOffsetX,
                        _cacheOffsetY:  sharedObject.createJsObject._cacheOffsetY,
                        _cacheScale:    sharedObject.createJsObject._cacheScale,
                        _cacheWidth:    sharedObject.createJsObject._cacheWidth,
                        _filterOffsetX: sharedObject.createJsObject._filterOffsetX,
                        _filterOffsetY: sharedObject.createJsObject._filterOffsetY
                    }
                } else res = 0; // No cache
                break;
            case "image":
                if (value) {
                    res = {
                        tagName: value.tagName,
                        src: value.getAttribute("src"),
                        assetId: value.assetId
                    };
                }
                break;
            case "SharedObject":
                // Store only the sharedId, as recreation is done by referencing to the separate SharedObject helper
                if (value) {
                    if (typeof value === "string") res = value; // SharedId is stored directly in value, e.g. after converting from Sendable
                    else if (value.sharedId) res = value.sharedId; // value is already monitored by a SharedObject, so that sharedId is used
                    else res = null; // No suitable sharedId found
                } else {
                    // No SharedObject helper set (e.g. value is null or undefined)
                    res = null;
                }
                break;
            case "ColorMatrix":
                if (value) {
                    if (Array.isArray(value)) res = value;
                    else res = value.toArray();
                }
                break;

            // Filter
            case "AlphaMapFilter":
            case "AlphaMaskFilter":
            case "BlurFilter":
            case "ColorFilter":
            case "ColorMatrixFilter":
                if (value) subProperties = filters[options.className].subProperties;
                break;

            // Usual types
            case "Graphics":
                if (value) subProperties = defaultSyncProperties.graphics.subProperties;
                break;
            case "Matrix2D":
                if (value) subProperties = defaultSyncProperties.transformMatrix.subProperties; // transformMatrix is of type Matrix2D
                break;
            case "Rectangle":
                if (value) subProperties = defaultSyncProperties._bounds.subProperties; // _bounds is of type Rectangle
                break;
            case "Shadow":
                if (value) subProperties = defaultSyncProperties.shadow.subProperties;
                break;

            // Graphic Commands
            case "Arc":
            case "ArcTo":
            case "BezierCurveTo":
            case "Circle":
            case "Ellipse":
            case "Fill":
            case "LineTo":
            case "MoveTo":
            case "PolyStar":
            case "QuadraticCurveTo":
            case "Rect":
            case "RoundRect":
            case "Stroke":
            case "StrokeDash":
            case "StrokeStyle":
                if (value) subProperties = graphics[options.className].subProperties;
                break;

            case "BeginPath":
            case "ClosePath":
                // Nothing to store
                break;
            case "style":
                if (value) {
                    if (typeof value === "string") res = value; // Fill or Stroke style defined by color string
                    else subProperties = graphics["Fill"].subProperties.style.subProperties; // gradient or bitmap style defined by props
                }
                break;
            case "styleProps":
                if (value) subProperties = graphics["Fill"].subProperties.style.subProperties.props.subProperties;
                res = {};
                break;

            // Primitive types
            case "Boolean":
            case "Number":
            case "String":
                res = value;
                break;
            case "Object":
                res = {}; // Blank object to prevent modifying the subProperties of the original object (e.g. Fill.style.props.colors)
                break;
            default:
                MTLG.warn("ddd", "SharedObject.serialize: No converting defined for className ", options.className);
                res = value;
                break;
        }
    } else {
        // Copy value if theres no class to initialize, e.g. for primitive values
        res = value;
    }

    // Attach subproperties
    if (subProperties) {
        for (let p in subProperties) {
            if (value !== null && value !== undefined && value.hasOwnProperty(p)) {
                res[p] = serialize(value[p], subProperties[p], sharedObject);
            }
        }
    }
    return res;
};


export {getPropertiesTemplate, serialize, deserialize, getClassName, instantiateClass};