/**
 * @Date:   2019-03-07T14:05:39+01:00
 * @Last modified time: 2019-08-07T11:29:05+02:00
 */



const app = require('express')();
const cors = require('cors');

app.use(cors());

const http = require('http').Server(app);


var io = require('socket.io')(http,
    {
        cors: {
            origin: "*",
            methods: ["GET", "POST"]
        }
    }
);


var dd = {
    /** Configuration */
    config: {
        /** Debug mode, more verbose output */
        debug: false,
        /** Port to listen on for new connections */
        port: process.env.PORT || 3125
    },

    /** Registered listeners for updates on SharedObjects are stored here */
    updateListeners: new Map() // Maps SharedObject.sharedId to Sets of listenerIDs
};

// Setup responses to connections and events
io.on('connection', function (socket) {
    // Log connections
    if (dd.config.debug) {
        console.log("user connected: " + socket.id);
    }

    /* Private helper functions */

    /**
     * Checks if a room with the given name exists and is an automatically generated personal room
     * @param {string} name The name of the room to check
     * @returns {boolean} True, if the room is a generated personal room
     */
    var isPersonalRoom = function (name) {
        // A room is a personal generated room if it exists and the name of the room is one of its members. rooms[name].sockets are the members of room "name"
        return io.sockets.adapter.rooms.hasOwnProperty(name) && io.sockets.adapter.rooms[name].sockets[name];
    };

    /**
     * Checks if a room with the given name exists and is no automatically generated personal room
     * @param {string} name The name of the room to check
     * @returns {boolean} True, if the room is a real room
     */
    var isRealRoom = function (name) {
        // A room is real if it exists and if is no automatically generated personal room. The name of the room is the name of one of its members for the generated personal ones.
        return io.sockets.adapter.rooms.hasOwnProperty(name) && !io.sockets.adapter.rooms[name].sockets[name];
    };

    /**
     * Check if this socket shares a room with the socket to send to
     * @param {string} to The id of the socket to send to
     * @returns {boolean} True, if this socket shares a room with the socket to send to
     */
    var sameRoom = function (to) {
        // For group message check if sender is member of the group
        if (io.sockets.adapter.rooms.hasOwnProperty(to) && io.sockets.adapter.rooms[to].sockets[socket.id]) {  // rooms[to].sockets are the members of room "to"
            return true;
        } else {
            // For personal message check if receiver is also member of a joined room
            for (var r in socket.rooms) {
                if (socket.rooms.hasOwnProperty(r) && io.sockets.adapter.rooms[r].sockets[to]) { // Is receiver in the room r as well
                    return true;
                }
            }
        }
        return false;
    };

    /*- Built in events -*/

    // User disconnected
    socket.on('disconnect', function () {
        if (dd.config.debug) console.log("user disconnected: " + socket.id);

        for (var [sharedId, listeners] of dd.updateListeners) { // Iterate over all SharedObjects
            listeners.delete(socket.id); // Remove the disconnected socket from the listeners
        }
    });

    /*- Rooms -*/

    // Creates a new room with name "name" if not already existing
    socket.on('createRoom', function (name, cb) {
        // Check if room already exists
        var rooms = io.sockets.adapter.rooms;
        for (var r in rooms) {
            if (rooms.hasOwnProperty(r) && r === name) {
                cb({ success: false, reason: "Room already exists", name: name });
                return;
            }
        }

        // Room or client does not exist, so the new room can be created
        socket.join(name, function (err) {
            if (err) cb({ success: false, reason: "Server error", name: name, details: err });
            else cb({ success: true, name: name });
        });
    });

    // Gets a list of all clients, filtered from all socket.io rooms
    socket.on('getAllClients', function (cb) {
        var res = [];
        var rooms = io.sockets.adapter.rooms;
        for (var name in rooms) {
            if (isPersonalRoom(name)) {
                res.push(name);
            }
        }
        cb(res);
    });

    // Gets a list of all manually created rooms, filtered from all socket.io rooms
    socket.on('getAllRooms', function (cb) {
        var res = {};
        var rooms = io.sockets.adapter.rooms;
        for (var name in rooms) {
            if (isRealRoom(name)) {
                res[name] = rooms[name];
            }
        }
        cb(res);
    });

    // Gets a list of all manually created rooms the socket is a member of
    socket.on('getJoinedRooms', function (cb) {
        var res = {};
        for (var name in socket.rooms) {
            if (isRealRoom(name)) {
                res[name] = io.sockets.adapter.rooms[name]; // io.sockets.adapter.rooms contains more details than socket.rooms, but the rooms are the same
            }
        }
        cb(res);
    });

    // The socket joins the room with name "name", if this room exists
    socket.on('joinRoom', function (name, cb) {
        if (isRealRoom(name)) {
            // Room exists and is no automatic personal, so it can be joined
            socket.join(name, function (err) {
                if (err) cb({ success: false, reason: "Server error", name: name, details: err });
                else cb({ success: true, name: name });
            });
        } else {
            // Room does not exist and cannot be joined
            cb({ success: false, reason: "Room does not exist", name: name });
        }
    });

    // The socket leaves the room with name "name", if this room exists
    socket.on('leaveRoom', function (name, cb) {
        if (isRealRoom(name)) {
            // Room exists and is no automatic personal, so it can be leaved
            socket.leave(name, function (err) {
                if (err) cb({ success: false, reason: "Server error", name: name, details: err });
                else cb({ success: true, name: name });
            });
        } else {
            // Room does not exist and cannot be leaved
            cb({ success: false, reason: "Room does not exist", name: name });
        }
    });

    /*- Data transfer -*/

    // The socket (either this or the given listenerId) is removed from the listeners for updates of the SharedObject with the given sharedId
    socket.on('deregisterUpdateListener', function (sharedId, listenerId) {
        listenerId = listenerId || socket.id;
        if (dd.updateListeners.has(sharedId)) {
            dd.updateListeners.get(sharedId).delete(listenerId); // Remove socket from the Set of listeners
        }
    });

    // The socket (either this or the given listenerId) is registered as a listener for updates of the SharedObject with the given sharedId
    socket.on('registerUpdateListener', function (sharedId, listenerId) {
        listenerId = listenerId || socket.id;
        if (!dd.updateListeners.has(sharedId)) dd.updateListeners.set(sharedId, new Set()); // Ensure Set of listeners exists for the SharedObjects Id
        dd.updateListeners.get(sharedId).add(listenerId); // Add socket to the Set of listeners
    });

    // The SharedObject with the given sharedId has changed (property from oldValue to newValue) and this change is transmitted to all listeners
    socket.on('sendUpdate', function (sharedId, propertyPath, oldValue, newValue, additionalInfo) {
		    if (dd.updateListeners.has(sharedId)) { // At least one listener
            // Prepare message to send
            var message = {
                action: "updateSharedObject",
                sharedId: sharedId,
                propertyPath: propertyPath,
                oldValue: oldValue,
                newValue: newValue,
                additionalInfo: additionalInfo
            };

            // Send update to all listeners. for..in does not work on Sets
            dd.updateListeners.get(sharedId).forEach(function (listener) {
              socket.to(listener).emit('receiveMessage', socket.id, listener, sameRoom(listener), message);
      				// Register UpdateListener, if it was an update to add helpers
              if (additionalInfo) {
        				if(additionalInfo.addHelpers){
        					for (var i in additionalInfo.addHelpers) {
        						if (additionalInfo.addHelpers[i].sharedId){
        							var sharedId = additionalInfo.addHelpers[i].sharedId
        							// Ensure Set of listeners exists for the SharedObjects Id
        							if (!dd.updateListeners.has(sharedId)) dd.updateListeners.set(sharedId, new Set());
        							dd.updateListeners.get(sharedId).add(listener); // Add socket to the Set of listeners
        						}
        					}
        				}
              }
            });
        }
    });

    // Sends the given "content" to the client or room with name "to". "isInSameRoom" is true, if the sender is a member of room "to" or in a same room as client "to"
    // The room name "all" can be used to send a message to all clients if debug is true. If debug is true, no separate room with the name "all" should be used.
    socket.on('sendMessage', function (to, content) {
        if(dd.config.debug && to === "all") socket.broadcast.emit('receiveMessage', socket.id, to, false, content); // Broadcasting to everyone for debugging, otherwise usual behavior
        else socket.to(to).emit('receiveMessage', socket.id, to, sameRoom(to), content); // Callback is not supported here
    });
});

// Wait for connections
http.listen(dd.config.port, function () {
    console.log('listening on *:' + dd.config.port);
});
